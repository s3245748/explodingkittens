# ExplodingKittens


Welcome to Exploding Kittens! A game made by Christina Braun and Marit van der Valk.

### Features

1. Play a full game of Exploding Kittens which determines a winner!	
2. Possible card actions include exploding, defuse, attack, favor, nope, shuffle, skip, see the future, cat combos of two and cat combos of three.
2.	Play locally on one computer with 2 to 5 players.
3.	Play on a network on the same or on multiple computers with 2 to 5 players. 
4.	Play against a computer player locally and on the network. 


### Starting a game 

#### Locally: 
1.	Run the class GameTUI.
2.	Type how many players will play (2-5).
3.	Give each player a name. Duplicate names are possible.

#### On the network:  
1.	Run the class GameServer
2.	Run the class GameClient for as many players that will play
3. In the GameServer console, type the number of players that will join (2-5) and type in the server port number 8888.
4. In each GameClient console, type in a name and press enter. Duplicate names are not possible.

If a GameServer is running on a different computer, remember to change the IP address accordingly in the GameClient class!

When the game ends, rerun the classes.

### Making Moves

#### Locally:
When it is your turn, type if you want to play or draw a card: “p” or “d”.
If you choose “p” for play, you have to indicate how many cards you want to put down: 
“s” for a single card, “p” for two (pair), “t” for three (triple).
If you are required to choose another player, you must type in their name. The name must match exactly, including upper and lowercase. 
For noping someone's action, type “y” for yes or “n” for no. 
All other actions are explained in the console.

#### On the network: 
When it is your turn, you will see your hand of cards and an index in front.
Type the index of the card you want to play for a single card or type “d” to draw. If you want to play a combo of 2 or 3 cards, type “c2” or “c3” accordingly.
You will see what moves are possible for you by reading what is indicated after the text “Options for you:”. Example: If you don’t have pairs then you cannot play “c2”. So the text will be “Options for you: “d” ”.
Some cards will require you to select other players. For this, a list of the other players will be displayed with an index in front. Type the index of the player you want to get a card from. 
For other input, read what it says in the console and give an appropriate input. Example: to nope someone, type “y” for yes or “n” for no.    

### Playing against a computer player

In the local game as well as the network game, type the name “COMPUTER” to play against a computer player. This way, a computer player is started. Keep in mind that on the networked game only one computer can be started as no duplicate names are possible. On the local game, you can play against as many computer players as your heart desires (as long as it is in the range between 2 and 5).  

## Enjoy the game!