package localgame.model.Test;

import localgame.model.Card;
import localgame.model.Deck;
import localgame.model.Hand;
import localgame.model.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;
import localgame.model.*;
import static org.junit.jupiter.api.Assertions.*;

public class GameTest {

    Game game;
    Deck deck;
    Player bob;
    Player alice;
    ArrayList<Player> players;


    @BeforeEach
    public void start(){
        /* Make players */
        ArrayList<Card> handBob = new ArrayList<>();
        bob = new Player("Bob", new Hand(handBob));
        ArrayList<Card> handAlice = new ArrayList<>();
        handAlice.add(new Card(Card.Type.NOPE));
        alice = new Player("Alice", new Hand(handAlice));
        players = new ArrayList<Player>();
        players.add(bob);
        players.add(alice);

        ArrayList<Card> deckCards = new ArrayList<>();
        deckCards.add(new Card(Card.Type.DEFUSE));
        deck = new Deck(deckCards);

        //Deck deck = new Deck(5);
        game = new Game(deck, players);
    }

    /**
     * Test for a section of the turn method in the Game class
     * tests correct behaviour if an exploding kitten is drawn
     */
    @Test
    public void testExplodingDrawing(){
        bob.setAttackBool(true);

        Player curr = game.getTurn(); //will be bob
        curr.setOutOfGame(true);
        String move = "d";

        if (move.equals("d")) {
            curr.drawCard(deck);
            if (curr.getOutOfGame()) { //simulate Exploding Kitten
                game.nextPlayer();
                game.getPlayers().remove(curr);
                //System.out.println("Sorry, no defuse Card. BOOOOOM! You are out of the game!!!");
            }
        }
        System.out.println(game.getTurn().getName());
        assertTrue(game.getTurn().getName().equals("Alice"));
        assertFalse(game.getPlayers().contains(bob));

    }

    /**
     * Tests if the current player can appropriately play a card and if all users that have a nope can nope the action
     */
    @Test
    public void testPlayingNope(){
        Player p = game.getTurn(); //bob
            String amountMove = "s";

            Card.Type originalMove = null;
            String tryMove = "";
            if (amountMove.equals("p")) {
                tryMove = " wanted to play two cards";
                //p.getHand().remove2(console);
                //p.rem2();
            }
            if (amountMove.equals("t")) {
                tryMove = " wanted to play three cards";
                //p.getHand().remove3(console);
                //p.rem3();
            }
            if (amountMove.equals("s")) {
                originalMove = Card.Type.ATTACK; //bob plays attack
                tryMove += " wanted to play " + originalMove;
                //p.playCard(originalMove);
            }
            game.makeNopeHavers();
            boolean asking = true;
            //If not exploding kitten or defuse. Go through all the players with nope cards and ask if they want to play it.
            while (asking && !tryMove.equals(" wanted to play " + Card.Type.DEFUSE.toString()) && !tryMove.equals(Card.Type.EXPLODING_KITTEN.toString())) {
                for (Player nopeP : game.getNopeHavers()) {
                    if (!nopeP.getName().equals(p.getName())) { //Alice has attack
                        boolean nopeAsking = true;
                        if (nopeP.getName().equals("COMPUTER")) {
                            //nopeAsking = nopeP.doNopeAsk(getTurn(), nopeP, tryMove);
                        } else {
                            nopeAsking = true; // Alice nopes
                        }
                        if (nopeAsking) {
                            game.setNopePutCount(game.getNopePutCount() + 1);
                            p.getHand().removeCard(Card.Type.NOPE);
                            game.makeNopeHavers();
                            tryMove += " noped by " + nopeP.getName();

                            if (game.getNopeHavers().isEmpty()) {
                                asking = false;
                            }
                        }
                    }
                }
                asking = false;
            }

            assertTrue(game.getNopePutCount() % 2 != 0);// Odd so card will not be played
    }


    @Test
    public void testAttack(){
            String amountMove = "s";
            Card.Type originalMove = Card.Type.ATTACK;

            boolean turn = true;
            Player p = game.getTurn();
            // System.out.println("MADE MOVE " + originalMove);
            if (amountMove.equals("p")) {
                p.twoSame(players);
            } else if (amountMove.equals("t")) {
                p.threeSame(deck, players);
            } else if (originalMove != null) {
                //Card c = p.playCard(draw, players);
                p.putCard(originalMove, deck, players);
            }

            assertTrue(p.getAttackBool());

            if (p.getAttackBool()) { //true
                p.setAttackBool(false);
                if (game.getAttackCount() == 1){
                    game.setAttackCount(2);
                }
                else {
                    game.setAttackCount(game.getAttackCount()+2);
                }
                Player curr = game.whoseNext();
                curr.setIsAttacked(true);
            }

            assertTrue(p.getSkipBool());
            assertFalse(p.getIsAttacked());

            if (p.getSkipBool() && p.getIsAttacked()) {
                if(game.getAttackCount() > 1){
                    game.setAttackCount(game.getAttackCount()-1);
                }
                else {
                    p.setIsAttacked(false);
                    p.setSkipBool(false);
                    turn = false;
                }
            }
            else if(p.getSkipBool() && game.getAttackCount() == 1){
                game.setAttackCount(game.getAttackCount()-1);
                p.setSkipBool(false);
                turn = false;
            }
            else if (p.getSkipBool()){
                //attackCount--;
                p.setSkipBool(false);
                turn = false;
            }
        assertTrue(game.getAttackCount() == 2);
        assertFalse(bob.getHand().contains(Card.Type.ATTACK));
        assertFalse(bob.getSkipBool());
        assertFalse(p.getSkipBool());
    }

    @Test
    public void nextPlayerTest(){
        assertTrue(game.getTurn() == bob);
        assertTrue(bob.getTurnBoolean());
        assertFalse(game.getTurn() == alice);
        assertFalse(alice.getTurnBoolean());

        game.nextPlayer();
        assertTrue(game.getTurn() == alice);
        assertTrue(alice.getTurnBoolean());
        assertFalse(game.getTurn() == bob);
        assertFalse(bob.getTurnBoolean());
    }

    @Test
    public void gameOverTest(){ //utelizes checkGame method
        assertFalse(game.checkGame());
        game.getPlayers().remove(bob);
        assertTrue(game.checkGame());
    }

}
