package localgame.model.Test;

import localgame.model.Card;
import localgame.model.Deck;
import localgame.model.Hand;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HandTest {

    ArrayList<Card> cardList = new ArrayList<>();
    Hand hand;
    @BeforeEach
    public void start(){
        cardList.add(new Card(Card.Type.TACO_CAT));
        cardList.add(new Card(Card.Type.TACO_CAT));
        cardList.add(new Card(Card.Type.HAIRY_POTATO_CAT));
        Hand hand = new Hand(cardList);
        this.hand = hand;
    }

    @Test
    public void testHasType(){
        assertTrue(hand.hasType(Card.Type.TACO_CAT));
        assertFalse(hand.hasType(Card.Type.DEFUSE));
    }

    @Test
    public void removeCardTest(){
        hand.removeCard(Card.Type.TACO_CAT);
        assertEquals(true, hand.getTypeHand().contains(Card.Type.TACO_CAT));
        hand.removeCard(Card.Type.HAIRY_POTATO_CAT);
        assertEquals(false, hand.getTypeHand().contains(Card.Type.HAIRY_POTATO_CAT));

        hand.removeCard(Card.Type.TACO_CAT);
        assertEquals(false, hand.getTypeHand().contains(Card.Type.TACO_CAT));
        assertTrue(hand.removeCard(Card.Type.TACO_CAT) == null);
    }

    @Test
    public void testReturnMultiples(){
        ArrayList<Card> cardsInHand = new ArrayList<>();
        cardsInHand.add(new Card(Card.Type.DEFUSE));
        cardsInHand.add(new Card(Card.Type.NOPE));
        cardsInHand.add(new Card(Card.Type.FAVOR));
        cardsInHand.add(new Card(Card.Type.SKIP));
        Hand h = new Hand(cardsInHand);

        assertEquals(h.returnMultiples()[0], 0);
        assertEquals(h.returnMultiples()[1], 0);

        cardsInHand.add(new Card(Card.Type.DEFUSE));
        assertEquals(h.returnMultiples()[0], 1);
        assertEquals(h.returnMultiples()[1], 0);

        cardsInHand.add(new Card(Card.Type.FAVOR));
        cardsInHand.add(new Card(Card.Type.FAVOR));
        cardsInHand.add(new Card(Card.Type.FAVOR));

        assertEquals(h.returnMultiples()[0], 2);
        assertEquals(h.returnMultiples()[1], 1);

        h.removeCard(Card.Type.DEFUSE);
        h.removeCard(Card.Type.DEFUSE);
        assertEquals(h.returnMultiples()[0], 1);
        assertEquals(h.returnMultiples()[1], 1);
    }
}
