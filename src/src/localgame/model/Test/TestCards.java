package localgame.model.Test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import localgame.model.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestCards {
    private ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    private ArrayList<Card> testDeck;
    private Deck deck;
    private Hand hand;
    @BeforeEach
    public void start(){
        //setup player hands with specific cards

        System.setOut(new PrintStream(outputStream));
        System.setOut(System.out);

        this.testDeck = new ArrayList<>();
        testDeck.add(new Card(Card.Type.TACO_CAT));
        testDeck.add(new Card(Card.Type.HAIRY_POTATO_CAT));
        testDeck.add(new Card(Card.Type.CATTER_MELON));
        testDeck.add(new Card(Card.Type.ATTACK));
        testDeck.add(new Card(Card.Type.FAVOR));
        deck = new Deck(testDeck);

        ArrayList<Card> handCards = new ArrayList<>();
        handCards.add(new Card(Card.Type.DEFUSE));
        handCards.add(new Card(Card.Type.FAVOR));
        handCards.add(new Card(Card.Type.SHUFFLE));
        handCards.add(new Card(Card.Type.TACO_CAT));
        handCards.add(new Card(Card.Type.TACO_CAT));
        handCards.add(new Card(Card.Type.CATTER_MELON));
        handCards.add(new Card(Card.Type.CATTER_MELON));
        handCards.add(new Card(Card.Type.CATTER_MELON));
        handCards.add(new Card(Card.Type.ATTACK));
        handCards.add(new Card(Card.Type.SKIP));
        handCards.add(new Card(Card.Type.SEE_THE_FUTURE));
        handCards.add(new Card(Card.Type.NOPE));

        hand = new Hand(handCards);
    }

    @Test
    public void testSeeThFuture(){
        deck.showTopThree();

        assertThat(outputStream.toString(), containsString("TACO_CAT"));
        assertThat(outputStream.toString(), containsString("HAIRY_POTATO_CAT"));
        assertThat(outputStream.toString(), containsString("CATTER_MELON"));
        outputStream.reset();

        testDeck.remove(0);
        testDeck.remove(0);
        deck.showTopThree();
        assertThat(outputStream.toString(), containsString("CATTER_MELON"));
        assertThat(outputStream.toString(), containsString("ATTACK"));
        assertThat(outputStream.toString(), containsString("FAVOR"));
        outputStream.reset();

        testDeck.remove(0);
        deck.showTopThree();
        assertThat(outputStream.toString(), containsString("ATTACK"));
        assertThat(outputStream.toString(), containsString("FAVOR"));
        outputStream.reset();
    }

    @Test
    public void testShuffle(){
        deck.shuffle();
        assertTrue(!testDeck.get(0).getType().equals(Card.Type.TACO_CAT) ||
                !testDeck.get(1).getType().equals(Card.Type.HAIRY_POTATO_CAT) ||
                !testDeck.get(2).getType().equals(Card.Type.CATTER_MELON));
    }

    @Test
    public void testNope(){
        /*TODO*/
    }
    @Test
    public void testFavor(){
        /*TODO*/
    }
    @Test
    public void testAttack(){
        /*TODO*/

    }
    @Test
    public void testSkip(){
        /*TODO*/
    }

    @Test
    public void testTwo(){
        /*TODO*/
    }
    @Test
    public void testThree(){
        /*TODO*/
    }
    @Test
    public void testExploding(){
        /*TODO*/
    }

}
