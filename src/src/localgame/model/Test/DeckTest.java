package localgame.model.Test;

import localgame.model.Card;
import localgame.model.Deck;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DeckTest {
    Deck deck = new Deck(5);

    @Test
    public void deckCards(){
        int defuse = 0;
        int attack = 0;
        int shuffle = 0;
        int exploding = 0;
        int favour = 0;
        int see = 0;
        int skip = 0;
        int nope = 5;

        for (Card c : deck.getDeck()) {
            if(c.getType().equals(Card.Type.DEFUSE)){
                defuse++;
            }
            if(c.getType().equals(Card.Type.ATTACK)){
                attack++;
            }
            if(c.getType().equals(Card.Type.SHUFFLE)){
                shuffle++;
            }
            if(c.getType().equals(Card.Type.FAVOR)){
                favour++;
            }
            if(c.getType().equals(Card.Type.SEE_THE_FUTURE)){
                see++;
            }
            if(c.getType().equals(Card.Type.EXPLODING_KITTEN)){
                exploding++;
            }
            if(c.getType().equals(Card.Type.SKIP)){
                exploding++;
            }
            if(c.getType().equals(Card.Type.NOPE)){
                exploding++;
            }
        }

        assertEquals(4,attack);
        assertEquals(4,shuffle);
        assertEquals(4,favour);
        assertEquals(5,see );
        assertEquals(5,nope );
        assertEquals(2,defuse);
    }
}
