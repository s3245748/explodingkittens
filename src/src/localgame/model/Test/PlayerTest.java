package localgame.model.Test;

import localgame.model.Card;
import localgame.model.Deck;
import localgame.model.Hand;
import localgame.model.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

public class PlayerTest {
    @BeforeEach
    public void start(){
        /*TODO: setup player hands with specific cards*/
    }

    @Test
    public void testAskMove(){
        Player p = new Player("Bob", new Hand(new ArrayList<>()));
        assertEquals("d", p.askMove());

        ArrayList<Card> hand = new ArrayList<>();
        hand.add(new Card(Card.Type.DEFUSE));
        hand.add(new Card(Card.Type.NOPE));

        Player p2 = new Player("Bob", new Hand(hand));
    }

    /**
     * Tests if the top card of the deck is returned (index 0)
     * Add card to hand if it is not an exploding kitten
     */
    @Test
    public void testDrawTopCard(){
        Player p = new Player("Bob", new Hand(new ArrayList<>()));
        ArrayList<Card> deck = new ArrayList<>();
        deck.add(new Card(Card.Type.HAIRY_POTATO_CAT));
        deck.add(new Card(Card.Type.TACO_CAT));
        deck.add(new Card(Card.Type.FAVOR));
        deck.add(new Card(Card.Type.EXPLODING_KITTEN));
        deck.add(new Card(Card.Type.NOPE));

        Deck gameDeck = new Deck(deck);
        assertEquals(new Card(Card.Type.HAIRY_POTATO_CAT).getType(),p.drawTopCard(gameDeck).getType());
        assertEquals(new Card(Card.Type.TACO_CAT).getType(),p.drawTopCard(gameDeck).getType());
        assertEquals(new Card(Card.Type.FAVOR).getType(),p.drawTopCard(gameDeck).getType());

        assertTrue(p.getHand().hasType(Card.Type.HAIRY_POTATO_CAT));
        assertTrue(p.getHand().hasType(Card.Type.TACO_CAT));
        assertTrue(p.getHand().hasType(Card.Type.FAVOR));

        assertEquals(new Card(Card.Type.EXPLODING_KITTEN).getType(),p.drawTopCard(gameDeck).getType());
        assertFalse(p.getHand().hasType(Card.Type.EXPLODING_KITTEN)); //card should not be in hand

        assertFalse(p.getNopeDraw());

        assertEquals(new Card(Card.Type.NOPE).getType(),p.drawTopCard(gameDeck).getType());
        assertTrue(p.getNopeDraw());
    }
}
