package localgame.model;

/**
 * Card class for local game Exploding Kittens
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */

public class Card {
    private Type type;

    public enum Type{
        EXPLODING_KITTEN,
        DEFUSE,
        ATTACK,
        FAVOR,
        NOPE,
        SHUFFLE,
        SKIP,
        SEE_THE_FUTURE,
        TACO_CAT,
        BEARD_CAT,
        CATTER_MELON,
        HAIRY_POTATO_CAT,
        RAINBOW_RALPHING_CAT;
    }

    //Constructor
    public Card(Type type){
        this.type = type;
    }

    //getters
    public Type getType(){
        return type;
    }

    //setters

    //methods
    public void defuse(){

    }
    public void attack(){

    }
    public void nope(){

    }
    public void favor(){

    }
    public void shuffle(){

    }
    public void skip(){

    }

}
