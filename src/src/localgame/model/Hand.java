package localgame.model;

import localgame.controller.Console;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Hand class for local game Exploding Kittens
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */

public class Hand {
    private ArrayList<Card> hand;
    private HashMap<Card.Type, Integer> cardTypeAmountCountMap;
    private ArrayList<Card.Type> doubleList;
    private ArrayList<Card.Type> triplesList;

    /**Constructor
     * @ensures one DefuseCard to the Hand
     * @ensures add 7 other random cards to hand (no Exploding Kittens) (total of 8 cards)
     * @param deck the deck from which the cards are drawn
     */
    public Hand(Deck deck){
        hand = new ArrayList<>();
        cardTypeAmountCountMap = new HashMap<>();
        hand.add(new Card(Card.Type.DEFUSE));
        doubleList = new ArrayList<>();
        triplesList = new ArrayList<>();
        //add random cards from the deck
        for(int i = 0; i < 7; i++){ //only 7 times because the defuse card is already in
            Card c = deck.getRandomCard();
            hand.add(c);
        }
    }

    public Hand(ArrayList<Card> hand){ //user for testing hand methods
        this.hand = hand;
    }

    /**
     * @return a string with all the cards that are in the hand
     */
    public String viewHand(){
        String res = "";
        for(int i = 0; i < getHandCards().size(); i++){
            Card.Type c = getHandCards().get(i).getType();
            int index = i + 1;
            res += ("" + index + ": " + c + "\n");
        }
        return res;
    }

    /**
     * Removes a card from the hand of type t (in case it exists in hand)
     * @param t type of the card that gets removed
     * @return teh removed card. If card is not in hand return null
     */
    public Card removeCard(Card.Type t){
        if(!handEmpty()) {
            for (Card c : hand) {
                if (c.getType().equals(t)) {
                    hand.remove(c);
                    return c;
                }
            }
        }
        //card is not in hand
        return null;
    }

    /**
     * creates a map cardTypeAmountCountMap that maps the type of card in the hand to the occurrences of this type (Type , int)
     * calls the method makeDoubleAndTripleList()
     */
    private void makeMap(){
        cardTypeAmountCountMap = new HashMap<>();
        for (Card c : hand) {
            if (cardTypeAmountCountMap.get(c.getType()) == null){
                cardTypeAmountCountMap.put(c.getType(), 1);
            }
            else {
                cardTypeAmountCountMap.put(c.getType(), cardTypeAmountCountMap.get(c.getType()) +1 );
            }
        }
        makeDoubleAndTripleList();
    }

    /**
     * make two lists one for doubles and one for triples
     * double list: stores all card types in the hand that occur 2 or more times
     * triple list: stores all card types in the hand that occur 3 or more times
     */
    private void makeDoubleAndTripleList(){
        doubleList = new ArrayList<>();
        triplesList = new ArrayList<>();
        for (Card.Type t : cardTypeAmountCountMap.keySet()){
            if (cardTypeAmountCountMap.get(t) >= 2 && !doubleList.contains(t)){
                doubleList.add(t);
            }
            if (cardTypeAmountCountMap.get(t) >= 3 && !triplesList.contains(t)){
                triplesList.add(t);
            }
        }
    }


    /**
     * @return array of two values. First value for amount of doubles. Second for amount of triples
     */
    public int[] returnMultiples(){
        makeMap();
        return new int[] {doubleList.size(), triplesList.size()};
    }


    /**
     * @return string of all card types in the double list
     */
    public String returnDoublesTypes(){
        String result = "Pairs: ";
        for (int i = 0; i < doubleList.size(); i++) {
            result += "\n" + (i+1) + ": " + doubleList.get(i);
        }
        return result;
    }

    /**
     *
     * @return string of all card types in the triple list
     */
    public String returnTriplesTypes() {
        String result = "Triples: ";
        for (int i = 0; i < triplesList.size(); i++) {
            result += "\n"+ (i + 1) + ": " + triplesList.get(i);
        }
        return result;
    }

    /**
     *
     * @return a random card in the hand
     * (used for the double card move)
     */
    public Card retrunRandomCard(){
        Random random = new Random();
        int index = random.nextInt(hand.size());
        return hand.get(index);
    }

    /**
     * Check if the hand has a card of type t
     */
    public boolean hasType(Card.Type t){
        for (Card c : getHandCards()){
            if(c.getType().equals(t)) {
                return true;
            }
        }
        return false;
    }

    /**
     * add a card to the hand
     * @param c Card that gets added to hand
     * (used for double and triple card move and favor)
     */
    public void addCard(Card c){
        hand.add(c);
    }

    public ArrayList<Card> getHandCards() {
        return hand;
    }

    /**
     * @param index
     * @return Card type form the double list at the index
     * (used to remove this card type 2 times when double is played)
     */
    public Card.Type getTypeDouble(int index){
        //makeMap();
        return doubleList.get(index);
    }
    /**
     * @param index
     * @return Card type form the triple list at the index
     * (used to remove this card type 3 times when triple is played)
     */
    public Card.Type getTypeTriple(int index){
        //makeMap();
        return triplesList.get(index);
    }

    public ArrayList<Card.Type> getDoubleList(){
        makeMap();
        return doubleList;
    }

    public ArrayList<Card.Type> getTripleList(){
        makeMap();
        return triplesList;
    }

    public boolean handEmpty(){
        return hand.isEmpty();
    }

    public boolean hasNope(){
        for ( Card c : hand){
            if (c.getType().equals(Card.Type.NOPE)){
                return true;
            }
        }
        return false;
    }

    /**
     * remove two cards from the hand of the type gotten by the double list and the index
     * @param console
     */
    public void remove2(Console console){
        int index = console.expectInt("What pair do you want to put down? \n" + returnDoublesTypes(), 1, getDoubleList().size());
        index = index-1;
        //System.out.println("intdex:" + index);
        for (Card.Type t : doubleList){
            System.out.println(t);
        }

        removeCard(getTypeDouble(index));
        removeCard(getTypeDouble(index));
    }

    /**
     * remove three cards from the hand of the type gotten by the double list and the index
     * @param console
     */
    public void remove3(Console console){
        int index = console.expectInt("What triple do you want to put down? \n" + returnTriplesTypes(), 1, getTripleList().size());
        index = index - 1;
        removeCard(getTypeTriple(index));
        removeCard(getTypeTriple(index));
        removeCard(getTypeTriple(index));
    }

    /**
     * check weather a type af card t is in the hand
     */
    public boolean contains(Card.Type t){
        return hand.contains(t);
    }

    /**
     * An index is inputted and the card type at that index in the hand is returned
     */
    public Card.Type convertToType(int index){
        return hand.get(index-1).getType();
    }

    public ArrayList<Card.Type> getTypeHand() {
        ArrayList<Card.Type> arrL = new ArrayList<>();
        for (Card c : hand){
            arrL.add(c.getType());
        }
        return arrL;
    }

}
