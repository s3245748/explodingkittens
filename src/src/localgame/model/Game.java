package localgame.model;

import localgame.controller.Console;

import java.util.ArrayList;

/**
 * Game class for local game Exploding Kittens
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */

public class Game {
    private ArrayList<Player> players;
    private Deck draw;
    private int playerCount; //amount of players in the game
    private ArrayList<Player> nopeHavers;
    private int nopePutCount;
    private int attackCount;
    private boolean playedAttack;

    private final Console console = new Console();


    /**Constructor
     * Ask how many players will play in this round and store user input invar playerAmount.
     */
    public Game(){
        players = new ArrayList<>();
        this.playerCount = inputPlayersCount();
        this.draw = new Deck(playerCount); //makes a new deck for x amount of players
        this.nopeHavers = new ArrayList<>();
        nopePutCount = 0;
        makePlayersWithName();
        makeNopeHavers();

        this.attackCount = 1;
        this.playedAttack = false;
    }

    public Game(Deck d, ArrayList<Player> pls){
        players = pls;
        this.playerCount = pls.size();
        this.draw = d;
        this.nopeHavers = new ArrayList<>();
        nopePutCount = 0;
        players.get(0).setTurn(true);
        makeNopeHavers();

        this.attackCount = 1;
        this.playedAttack = false;
    }

    /** Getters and Setters**/
    public ArrayList<Player> getNopeHavers(){
        return nopeHavers;
    }
    public int getNopePutCount(){
        return nopePutCount;
    }
    public void setNopePutCount(int num){
        this.nopePutCount = num;
    }
    public int getAttackCount(){
        return attackCount;
    }
    public void setAttackCount(int num){
        this.attackCount = num;
    }

    /**
     * ask the user how many players are in the game
     * console checks validity of input (must be int else ask again)
     * @return inputted number
     */
    public int inputPlayersCount(){
        int amount = console.expectInt("How many players will play the game? (2 to 5)", 2, 5);
        System.out.println("Nice. " + amount + " players in this game." );
        return amount;
    }

    /**
     * Ask for as many player as there are in the game what the name is.
     * Make new player and add him/her to the players ArrayList
     * @requires playerCount != null && playerCount != 0
     * set the turn of the first player in list to be true
     */
    public void makePlayersWithName(){
        for(int i = 0; i < playerCount; i++){
            int pCounter = i+1;
            String name = console.readString("What is the name of player " + pCounter + "?");
            System.out.println();
            addPlayer(name);
            int left = playerCount-i-1;
            if(left > 0) {
                System.out.println("added " + name + ". " + left + " players left to input");
            }
        }
        players.get(0).setTurn(true);
    }


    /**
     * Add players to players arraylist and make a hand. If the name is "COMPUTER" make a computer player
     * @param name of the player
     */
    public void addPlayer(String name){
        Player player;
        if (name.equals("COMPUTER")){
            player = new ComputerPlayer(name, new Hand(draw));
        }
        else {
            player = new Player(name, new Hand(draw));
        }
        players.add(player);
    }


    public void start(){
        boolean gameOver = false;
        while (!gameOver){
            Player p = getTurn();
            if (attackCount == 0){
                attackCount++;
            }
                turn(p);
            if(!p.getOutOfGame()){
                nextPlayer();
            }
            else{
                p.setOutOfGame(false);
            }
            gameOver = checkGame();
        }
    }

    /**
     * Logic for the turn of one player
     * @param p Player whose turn it currently is
     */
    public void turn(Player p) {
        //first ask if player wants to put down a card
        //if no draw a card
        // else show cards and make user select a card
        makeNopeHavers();
        Player curr = p;
        boolean turn = true;

        while (0 < attackCount && curr == p){
            turn = true;
            while(turn){
                System.out.println();
            String move = p.askMove();
            if (move.equals("d")) {
                p.drawCard(draw);
                if (p.getOutOfGame()) {
                    nextPlayer();
                    players.remove(p);
                    System.out.println("Sorry, no defuse Card. BOOOOOM! You are out of the game!!!");
                    attackCount = 1;
                }
                if (p.getNopeDraw()){
                    if(!nopeHavers.contains(p)) {
                        nopeHavers.add(p);
                    }
                }
                //turn ends
                turn = false;
                attackCount--;
            }
            else if (move.equals("p")) { //put down a card
                String amountMove = p.askAmountMove();
                Card.Type originalMove = null;
                String tryMove = "";
                if (amountMove.equals("p")) {
                    tryMove = " wanted to play two cards";
                    p.rem2();
                }
                if (amountMove.equals("t")) {
                    tryMove = " wanted to play three cards";
                    p.rem3();
                }
                if (amountMove.equals("s")) {
                    originalMove = p.chooseCard();
                    tryMove += " wanted to play " + originalMove;
                    p.playCard(originalMove);
                }
                makeNopeHavers();
                boolean asking = true;
                //If not exploding kitten or defuse. Go through all the players with nope cards and ask if they want to play it.
                while (asking && !tryMove.equals(" wanted to play " + Card.Type.DEFUSE.toString()) && !tryMove.equals(Card.Type.EXPLODING_KITTEN.toString())) {
                    for (Player nopeP : nopeHavers) {
                        if (!nopeP.getName().equals(p.getName())) {
                            boolean nopeAsking = true;
                            if (nopeP.getName().equals("COMPUTER")) {
                                nopeAsking = nopeP.doNopeAsk(getTurn(), nopeP, tryMove);
                            } else {
                                nopeAsking = p.doNopeAsk(getTurn(), nopeP, tryMove);
                            }
                            if (nopeAsking) {
                                nopePutCount++;
                                p.getHand().removeCard(Card.Type.NOPE);
                                makeNopeHavers();
                                tryMove += " noped by " + nopeP.getName();

                                if (nopeHavers.isEmpty()) {
                                    asking = false;
                                }
                            }
                        }
                    }
                    asking = false;
                }

                if (nopePutCount % 2 == 0) { // even so card is played
                    if (amountMove.equals("p")) {
                        p.twoSame(players);
                    } else if (amountMove.equals("t")) {
                        p.threeSame(draw, players);
                    } else if (originalMove != null) {
                        p.putCard(originalMove, draw, players);
                    }

                    if (p.getAttackBool()) {
                        p.setAttackBool(false);
                        if (attackCount == 1){
                            attackCount = 2;
                        }
                        else {
                            attackCount += 2;
                        }
                        curr = whoseNext();
                        curr.setIsAttacked(true);
                    }

                    if (p.getSkipBool() && p.getIsAttacked()) {
                        if(attackCount > 1){
                            attackCount--;
                        }
                        else {
                            p.setIsAttacked(false);
                            p.setSkipBool(false);
                            turn = false;
                        }
                    }
                    else if(p.getSkipBool() && attackCount == 1){
                        attackCount--;
                        p.setSkipBool(false);
                        turn = false;
                    }
                    else if (p.getSkipBool()){
                        //attackCount--;
                        p.setSkipBool(false);
                        turn = false;
                    }
                } else {
                    System.out.println("Your card was noped. No action");
                }
                nopePutCount = 0;
            }
            }
        }
    }

    /**
     * Sets the turn of the current player to false and the turn of the next player in the list to true
     * @requires getTurn of one player in players to be true
     * @ensures getTurn of next player in list to be true
     * @ensures getTurn of current getTurn to be false
     */
    public void nextPlayer(){
        for(int i = 0; i < players.size(); i++){
            int index = 0;
            if(players.get(i).getTurnBoolean()){ //it is player i's turn
                index = (i + 1) % players.size();
                players.get(index).setTurn(true);
                players.get(i).setTurn(false);
                break;
            }
        }
    }

    public Player whoseNext(){
        for(int i = 0; i < players.size(); i++){
            int index = 0;
            if(players.get(i).getTurnBoolean()){ //it is player i's turn
                index = (i + 1) % players.size();
                return players.get(index);
            }
        }
        return null;
    }

    /**
     * returns player whose turn it is
     */
    public Player getTurn(){
        for(Player p : players){
            if(p.getTurnBoolean()){
              return p;
            }
        }
        return null;
    }

    /**
     * Check whether the game is over (if only one player left)
     */
    public boolean checkGame(){
        if (players.size() <= 1){
            System.out.println("Game over! Player " + players.get(0).getName() + " won!");
            return true;
        }
        return false;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }


    /**
     * Go through playerList. If they have a nope add to the nopeHavers list
     */
    public void makeNopeHavers(){
        nopeHavers = new ArrayList<>();
        for (Player p : players){
            if (p.getHand().hasNope() && !nopeHavers.contains(p)){
                nopeHavers.add(p);
            }
        }
    }
}
