package localgame.model;

import java.util.ArrayList;
import java.util.Random;

/**
 * ComputerPlayer class for local game Exploding Kittens
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */

public class ComputerPlayer extends Player {
    Random random;
    public ComputerPlayer(String name, Hand hand) {
        super(name, hand);
        this.random = new Random();
    }

    @Override
    public void putCard(Card.Type type, Deck deck, ArrayList<Player> players){
        switch (type) {
            case ATTACK: super.attack();
                break;
            case FAVOR: favor(players);
                break;
            case NOPE: nope();
                break;
            case SHUFFLE: super.shuffle(deck);
                break;
            case SKIP: skip();
                break;
            case SEE_THE_FUTURE: seeTheFuture(deck);
                break;
            default:
                break;
        }
    }
    @Override
    public String askMove(){
        System.out.println();
        System.out.println("It is " + getName() + "'s turn. This is your hand: ");
        int rand = random.nextInt(2);
        String[] arr = {"p", "d"};
        System.out.println("Played " + arr[rand]);
        return arr[rand];
    }

    @Override
    public Card.Type chooseCard(){
        int handSize = getHand().getHandCards().size();
        int rand = random.nextInt(handSize)+1;
        int counter = 0;
        while ( getHand().convertToType(rand) == Card.Type.DEFUSE && counter != handSize){ //don't chose defuse
            rand = random.nextInt(handSize)+1;
            counter++;
        }
        System.out.println("Chose " + getHand().convertToType(rand));
        return getHand().convertToType(rand);
    }

    /**
     * If player has defuse card then play it
     * @param deck
     */
    @Override
    public void isExploding(Deck deck) {
        if (getHand().hasType(Card.Type.DEFUSE)) {
            getHand().removeCard(Card.Type.DEFUSE); //remove defuse card
            setOutOfGame(false); //player does not get kicked out of game
            int rand = random.nextInt(deck.getDeck().size());
            deck.putCardAtIndex(rand, new Card(Card.Type.EXPLODING_KITTEN)); //add card to index in deck
        }
        else {
            setOutOfGame(true);
        }
    }

    @Override
    public Player returnVictim(ArrayList<Player> players, String input){
        String victimName = "";
        while (true){
            int rand = random.nextInt(players.size());
            victimName = players.get(rand).getName();
            for (Player p : players) {
                if (p.getName().equals(victimName) && !victimName.equals(this.getName())) {
                    System.out.println("Victim chosen is " + p.getName());
                    return p;
                }
            }
        }
    }

    @Override
    public String askAmountMove(){
        ArrayList<String> arr = new ArrayList<>();
        arr.add("s");
        if (getHand().returnMultiples()[0] > 0) {
            arr.add("p");
        }
        if (getHand().returnMultiples()[1] > 0) {
            arr.add("t");
        }
        int rand = random.nextInt(arr.size());
        System.out.println("Chosen move is " + arr.get(rand));
        return arr.get(rand);
    }

    @Override
    public void threeSame(Deck deck, ArrayList<Player> players){
        Player victim = returnVictim(players, "" );
        int chosenIndex = random.nextInt(12)+1;
        Card.Type chosenType = deck.returnAbriviationCardType(chosenIndex);
        if (victim.getHand().hasType(chosenType)){
            System.out.println("Your victim has the card! Now you get a " + chosenType + " card.");
            victim.getHand().removeCard(chosenType);
            getHand().addCard(new Card(chosenType));
        }
        else{
            System.out.println("Sorry, your victim does not have a " + chosenType + " card.");
        }
    }

    @Override
    public void favor(ArrayList<Player> players){
        Player victim= returnVictim(players, "");
        int index = random.nextInt(victim.getHand().getHandCards().size()+1);
        Card.Type type = victim.getHand().convertToType(index);
        victim.getHand().removeCard(type);
        getHand().addCard(new Card(type));
        System.out.println("Received " + type);
    }
    @Override
    public boolean doNopeAsk(Player turnP, Player nopeHaver, String tryMove){
        return false;
    }

    @Override
    public void rem2(){
        int dSize = getHand().getDoubleList().size();
        int index = random.nextInt(dSize);
        getHand().removeCard(getHand().getTypeDouble(index));
        getHand().removeCard(getHand().getTypeDouble(index));

        System.out.println("Played pair of " + getHand().getTypeDouble(index));
    }

    @Override
    public void rem3(){
        int dSize = getHand().getTripleList().size();
        int index = random.nextInt(dSize);
        getHand().removeCard(getHand().getTypeTriple(index));
        getHand().removeCard(getHand().getTypeTriple(index));
        getHand().removeCard(getHand().getTypeTriple(index));

        System.out.println("Played triple of " + getHand().getTypeTriple(index));

    }
}
