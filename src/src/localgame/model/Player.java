package localgame.model;

import localgame.controller.Console;

import java.util.ArrayList;

/**
 * Player class for local game Exploding Kittens
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */

public class Player {
    private String name;
    private Hand hand;
    private boolean turn;
    private boolean outOfGame;
    private boolean nopeDraw;
    private boolean skipBool;
    private boolean attackBool;
    private boolean isAttacked;
    private final Console console = new Console();


    // Constructor
    public Player(String name, Hand hand){
        this.name = name;
        this.turn = false;
        this.hand = hand;
        this.outOfGame = false;
        this.attackBool = false;
        this.nopeDraw = false;
    }

    /**Getters And Setter**/
    public void setTurn(boolean turn){
        this.turn = turn;
    }
    public boolean getTurnBoolean(){
        return turn;
    }
    public Hand getHand(){
        return hand;
    }
    public void setAttackBool(boolean attackBool){
        this.attackBool = attackBool;
    }
    public boolean getAttackBool(){
        return attackBool;
    }
    public void setIsAttacked(boolean bool){
        isAttacked = bool;
    }
    public boolean getIsAttacked(){
        return isAttacked;
    }


    public void setOutOfGame(boolean exploded){
        this.outOfGame = exploded;
    }
    public boolean getOutOfGame(){
        return outOfGame;
    }

    public void setNopeDraw(boolean nopeBool){
        this.nopeDraw = nopeBool;
    }
    public boolean getNopeDraw(){
        return nopeDraw;
    }

    public void setSkipBool(boolean b){
        this.skipBool = b;
    }
    public boolean getSkipBool(){
        return skipBool;
    }
    public String getName() {
        return name;
    }

    /**
     * Player has chosen a card to put down (as an index). Then get the type of card
     * Method calls corresponding player action methods depending on type of card
     */
    public void putCard(Card.Type type, Deck deck, ArrayList<Player> players){
        switch (type) {
            case ATTACK: attack();
                break;
            case FAVOR: favor(players);
                break;
            case NOPE: nope();
                break;
            case SHUFFLE: shuffle(deck);
                break;
            case SKIP: skip();
                break;
            case SEE_THE_FUTURE: seeTheFuture(deck);
                break;
            default:
                break;
        }
    }


    /**
     * Gets the top Card from the deck (Card at index 0)
     * if it is an exploding kitten don't put it in the hand
     * else do
     * @param deck
     * @return Card c at the top of the deck
     */
    public Card drawTopCard(Deck deck){
        Card c = deck.getTopCard();
        if(!c.getType().equals(Card.Type.EXPLODING_KITTEN)){
            hand.getHandCards().add(c);
        }
        if(c.getType().equals(Card.Type.NOPE)){
            nopeDraw = true;
        }
        return c;
    }

    /**
     * ask player if he/she wants to draw or put down a card
     * @return String ("p" or "d")
     */
    public String askMove(){
        if(getHand().handEmpty()){
            System.out.println("You have no cards. You have to draw");
            return "d";
        }
        return console.expectString("It is " + getName() + "'s turn. This is your hand: " + "\n" + hand.viewHand() + "\n" + "Do you want to play or draw a card? (p/d)" , new String[] {"p", "d"}, "Choose p or d");
    }


    public Card.Type chooseCard(){
        int index = console.expectInt("chose a card \n" + hand.viewHand(), 1, hand.getHandCards().size());
        return hand.convertToType(index);
    }
    public Card playCard(Card.Type type){
        return hand.removeCard(type);
    }


    /**
     * draw the top card of the deck
     * @param draw the deck from which card is drawn
     * if exploding kitten start actions to handle the exploding kitten card
     */
    public void drawCard(Deck draw){
        Card c = drawTopCard(draw);
        System.out.println("the card you drew is " + c.getType());
        if (c.getType().equals(Card.Type.EXPLODING_KITTEN)) {
            isExploding(draw);
        }
    }

    /**
     * Handles if the user draws an exploding kitten
     * if there is a defuse card then ask if player wants to put it
     * -> if no out of game
     * -> if yes player must say where in deck card goes
     * if no defuse, player gets kicked out of game
     * @param deck the deck that is used in the game
     */
    public void isExploding(Deck deck) {
        if (hand.hasType(Card.Type.DEFUSE)) {
            String move = console.expectString("You have a defuse card. Do you want to play it? (y/n)", new String[] {"y", "n"}, "Choose y or n");
            if (move.equals("y")) {
                hand.removeCard(Card.Type.DEFUSE); //remove defuse card
                outOfGame = false; //player does not get kicked out of game
                int pos = console.expectInt(getName() + ", you may put the card in a chosen position in the deck. The deck has " + deck.getDeck().size() + " Cards. 0 is the top card. What position should the card go?", 0, deck.getDeck().size());
                deck.putCardAtIndex(pos, new Card(Card.Type.EXPLODING_KITTEN)); //add card to index in deck
            } else {
                outOfGame = true;
            }
        }
        else {
            outOfGame = true;
        }
    }


    /**
     * reads the name of the victim from the user input and checks if it is a valid player. If not question asked again
     * @param players the list of players in the game
     * @param input the input of the current player
     * @return the player who is the victim
     */
    public Player returnVictim(ArrayList<Player> players, String input){
        String victimName = "";
        while (true){
            victimName = console.readString(input);
            for (Player p : players) {
                if (p.getName().equals(victimName) && !victimName.equals(this.name)) {
                    return p;
                }
            }
            System.out.println("Victim name invalid. Try again");
        }
    }


    //Player Actions From Card
    public void seeTheFuture(Deck deck){
        deck.showTopThree();
    }
    public void skip(){
        setSkipBool(true);
    }

    /**
     * Used for double and triple cards.
     * String array is made with possible moves
     * s for single card. d for double. t for triple
     * Ask for user input. Only string in the strint array are possible inputs
     * @return
     */
    public String askAmountMove(){
        ArrayList<String> arr = new ArrayList<>();
        arr.add("s");
        if (getHand().returnMultiples()[0] > 0) {
            arr.add("p");
        }
        if (getHand().returnMultiples()[1] > 0) {
            arr.add("t");
        }
        String[] str = new String[arr.size()];
        String possible = "possible choices: ";
        for (int i = 0; i < arr.size(); i++) {
            str[i] = arr.get(i);
            possible += arr.get(i) + " ";
        }
        return console.expectString( getOptionsFromConsole(), str, possible);
    }

    /**
     * Calls method that checks how many double and triples there are
     * @return
     */
    public String getOptionsFromConsole() {
        return console.printOptions(getHand());
    }

    /**
     * choose cards to put down (a pair) and remove two times from hand
     * choose player to steal card from
     * remove card from victim hand
     * add card to hand
     * @param players
     */
    public void twoSame(ArrayList<Player> players){
        Player p = returnVictim(players, getName() + ", you get to steal a random card from a player. Who is your victim?");
        Card c = p.hand.retrunRandomCard(); //get a random card
        System.out.println("The card you stole is " + c.getType());
        p.hand.removeCard(c.getType()); //remove from victim
        hand.addCard(c); //add to this player
    }

    /**
     * choose cards to put down (a tripe) and remove three times from hand
     * choose player to steal card from
     * show possible card and chose one
     *
     * remove card from victim hand
     * add card to hand
     * @param players
     */
    public void threeSame(Deck deck, ArrayList<Player> players){
        Player victim = returnVictim(players, getName() + ", you get to steal a chosen card from a player. Who is your victim?");
        int chosenIndex = console.expectInt("What card do you want? \n" + deck.abbreviations(), 1, 12);
        Card.Type chosenType = deck.returnAbriviationCardType(chosenIndex);

        if (victim.hand.hasType(chosenType)){
            System.out.println("Your victim has the card! Now you get a " + chosenType + " card.");
            victim.hand.removeCard(chosenType);
            hand.addCard(new Card(chosenType));
        }
        else{
            System.out.println("Sorry, your victim does not have a " + chosenType + " card.");
        }
    }

    /**
     * shuffles the deck
     * @param deck
     */
    public void shuffle(Deck deck){
        deck.shuffle();
    }

    /**
     * chose a player to give a card to. Select a card to give
     * remove card from hand. Add card to other player
     * @param players
     */
    public void favor(ArrayList<Player> players){
        Player victim = returnVictim(players, getName() + ", you may chose a player to give you a card. Who do you chose");
        int index = console.expectInt("It's player " + victim.getName() + "'s Turn.\nPlayer " + name + " gets a card from you. Which card do you want to give? \n" + victim.getHand().viewHand(), 1, victim.getHand().getHandCards().size());
        Card.Type type = victim.getHand().convertToType(index);
        victim.getHand().removeCard(type);
        hand.addCard(new Card(type));
        System.out.println("Player " + name + ", you got a " + type + " card.");
    }

    public void nope(){

    }

    public void attack(){
        skipBool = true;
        attackBool = true;
    }
    public boolean doNopeAsk(Player turnP, Player nopeHaver, String tryMove){
        System.out.println("Player " + nopeHaver.getName() + ", you have a Nope.");

        String answer = console.expectString("Player " + turnP.getName()  + tryMove + ". Do you want to nope this. (y,n)", new String[]{"y", "n"},  "y or n (yes or no)" );
        if (answer.equals("y")){
            return true;
        }
            return false;
    }

    public void rem2(){
        hand.remove2(console);
    }
    public void rem3(){
        hand.remove3(console);
    }
}
