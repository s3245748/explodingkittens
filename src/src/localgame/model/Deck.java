package localgame.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

/**
 * Deck class for local game Exploding Kittens
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */

public class Deck {
    private ArrayList<Card> deck;
    private HashMap<Integer, Card.Type> abbreviationsMap;

    /**
     * make a Deck with playerAmount-1 ExplodingKitten cards,
     * 2 defuse cards
     * 4 Attack, 4 Favor etc....
     * @param playersAmount in order to calculate amount of exploding kittens in deck
     */
    public Deck(int playersAmount){
        this.abbreviationsMap = new HashMap<>();
        makeAbbreviationsMap();
        deck = new ArrayList<>();
        for(int i = 0; i < playersAmount; i++){
            deck.add(new Card(Card.Type.EXPLODING_KITTEN));
        }
        deck.add(new Card(Card.Type.DEFUSE));
        deck.add(new Card(Card.Type.DEFUSE));

        for(int i = 0; i < 4; i++){
            deck.add(new Card(Card.Type.ATTACK));
            deck.add(new Card(Card.Type.FAVOR));
            deck.add(new Card(Card.Type.SHUFFLE));
            deck.add(new Card(Card.Type.SKIP));
            deck.add(new Card(Card.Type.TACO_CAT));
            deck.add(new Card(Card.Type.HAIRY_POTATO_CAT));
            deck.add(new Card(Card.Type.CATTER_MELON));
            deck.add(new Card(Card.Type.BEARD_CAT));
            deck.add(new Card(Card.Type.RAINBOW_RALPHING_CAT));

        }
        for(int i = 0; i < 5; i++){
            deck.add(new Card(Card.Type.NOPE));
            deck.add(new Card(Card.Type.SEE_THE_FUTURE));
        }
        shuffle();
    }

    public Deck(ArrayList<Card> deck){
        this.deck = deck;
    }

    /**
     * gets and removes first card
     * @return top card
     */
    public Card getTopCard(){
        Card c = deck.get(0);
        deck.remove(c);
        return c;
    }

    /**
     * insert a card at an index in the deck
     */
    public void putCardAtIndex(int index, Card card){
        deck.add(index, card);
    }

    /**
     * picks a random card and removes it from the deck. (Not Exploding kittens)
     * used to make the hand
     */
    public Card getRandomCard(){
        Random random = new Random();
        int r = random.nextInt(deck.size()-1);
        Card c = deck.get(r);

        //can't be an exploding kitten
        while (c.getType().equals(Card.Type.EXPLODING_KITTEN)){
            r = random.nextInt(deck.size()-1);
            c = deck.get(r);
        }
        deck.remove(deck.get(r));

        return c;
    }

    public void shuffle() {
        Collections.shuffle(deck);
    }

    /**
     * used for see_the_future card
     */
    public void showTopThree(){
        if(deck.size() > 2) {
            System.out.println(deck.get(0).getType());
            System.out.println(deck.get(1).getType());
            System.out.println(deck.get(2).getType());
        }
        else {
            for (int i = 0; i < deck.size(); i++) {
                System.out.println(deck.get(i).getType());
            }
        }
    }

    /**
     * returns a string of all card types and their allocated number
     */
    public String abbreviations(){
        String res = "";
        for (int ind : abbreviationsMap.keySet()){
            res += (abbreviationsMap.get(ind) + ": " + ind + "\n");
        }
        return res;
    }

    /**
     * converts an index to the corresponding card type according to abbreviationsMap
     */
    public Card.Type returnAbriviationCardType(int index){
        return abbreviationsMap.get(index);
    }

    /**
     * Map that allocates a number to a card type
     */
    public void makeAbbreviationsMap(){
        abbreviationsMap.put(1, Card.Type.TACO_CAT);
        abbreviationsMap.put(2, Card.Type.HAIRY_POTATO_CAT);
        abbreviationsMap.put(3, Card.Type.CATTER_MELON);
        abbreviationsMap.put(4, Card.Type.BEARD_CAT);
        abbreviationsMap.put(5, Card.Type.RAINBOW_RALPHING_CAT);
        abbreviationsMap.put(6, Card.Type.DEFUSE);
        abbreviationsMap.put(7, Card.Type.SEE_THE_FUTURE);
        abbreviationsMap.put(8, Card.Type.SKIP);
        abbreviationsMap.put(9, Card.Type.SHUFFLE);
        abbreviationsMap.put(10, Card.Type.FAVOR);
        abbreviationsMap.put(11, Card.Type.NOPE);
        abbreviationsMap.put(12, Card.Type.ATTACK);
    }


    /** Getters and Setters**/
    public ArrayList<Card> getDeck() {
        return deck;
    }

}
