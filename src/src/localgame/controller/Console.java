package localgame.controller;

import localgame.model.Hand;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

/**
 * Console class for local game Exploding Kittens
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */

public class Console{

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private PrintStream out = System.out;

    public String readString(String question){
        out.println(question);
        String answer = null;
        try {
            answer = reader.readLine();
        }
        catch (IOException e){
            out.println("Error reading");
            e.printStackTrace();
        }
        if (answer == null){
            return "";
        }
        return answer;
    }

    public int readInt(String question){
        int num = 0;
        while (true){
            try {
                num = Integer.parseInt(readString(question));
                return num;
            }
            catch (NumberFormatException e){
                out.println("Not an integer.");
            }
        }
    }

    /**
     * Method that checks that the user inputs a valid answer. (Example to draw or put a card the input must be either "d" or "p")
     * @param question text for the user to know what to input
     * @param expectVal array of possible answers. If qiven answer is not in the array ask again
     * @return
     */
    public String expectString(String question, String[] expectVal, String error){
        String ans = "";
        System.out.println(question);
        while (true){
            ans = readString("");
            for (String s : expectVal){
                if (s.equals(ans)){
                    return ans;
                }
            }
            System.out.println(error);
        }
    }

    public int expectInt(String question, int min, int max){
        int ans = 0;
        while (true){
            ans = readInt(question);
                if (ans <= max && ans >= min){
                    return ans;
                }
            System.out.println("range must be between: " + min + " and " + max);
        }
    }

    public String printOptions(Hand h){
        int pairs = h.returnMultiples()[0];
        int triples = h.returnMultiples()[1];
        String message = "You can play a pair (p). You can play a triple (t). You can play a single card (s)";
        if (pairs > 0 && triples == 0) {
            message = "You have " + pairs + " pairs. " + "You can play a pair (p). You can play a single card (s). \n" + h.returnDoublesTypes();
        } else if (pairs == 0 && triples > 0) {
            message = "You have " + triples + " triples. " + "You can play a triple (t). You can play a single card (s). \n" + h.returnTriplesTypes();
        } else if (pairs > 0 && triples > 0) {
            message = "You have " + pairs + " pairs and " + triples + " triples. " + message + "\n" + h.returnDoublesTypes() + "\n" + h.returnTriplesTypes();
        }
        else {
            message = "You have 0 pairs and 0 triples. You can only play a single card (s)";
        }
       return "This is your hand: " + "\n" + h.viewHand() + "\n" + message;
    }



}