package localgame.view;

import localgame.model.Game;

/**
 * GameTUI for local game Exploding Kittens
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */

public class GameTUI {
    public static void main(String[] args) {
        Game game = new Game();
        game.start();
    }

}
