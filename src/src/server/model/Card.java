package server.model;

/**
 * Card class for Networked Game Exploding Kittens
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */

public class Card {
    private Type type;

    public enum Type{
        EXPLODING_KITTEN,
        DEFUSE,
        ATTACK,
        FAVOR,
        NOPE,
        SHUFFLE,
        SKIP,
        SEE_THE_FUTURE,
        TACO_CAT,
        HAIRY_POTATO_CAT,
        CATTER_MELON,
        BEARD_CAT,
        RAINBOW_RALPHING_CAT
    }

    // Constructor
    public Card(Type type){
        this.type = type;
    }

    // Getters
    public Type getType(){
        return type;
    }


}
