package server.model;

import server.controller.GameServer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

/**
 * Deck class for Networked Game Exploding Kittens
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */

public class Deck {
    private ArrayList<Card> deck;
    private HashMap<Integer, Card.Type> abbreviationsMap;

    /**
     * Make a Deck with playerAmount-1 ExplodingKitten cards,
     * 2 defuse cards
     * 4 Attack, 4 Favor etc....
     * @param playersAmount in order to calculate amount of exploding kittens in deck
     * @ensures a deck is made with playerAmount-1 Exploding Kitten cards
     */
    public Deck(int playersAmount){
        this.abbreviationsMap = new HashMap<>();
        makeAbbreviationsMap();
        deck = new ArrayList<>();
        for(int i = 0; i < playersAmount; i++){
            deck.add(new Card(Card.Type.EXPLODING_KITTEN));
        }
        deck.add(new Card(Card.Type.DEFUSE));
        deck.add(new Card(Card.Type.DEFUSE));

        for(int i = 0; i < 4; i++){
            deck.add(new Card(Card.Type.ATTACK));
            deck.add(new Card(Card.Type.FAVOR));
            deck.add(new Card(Card.Type.SHUFFLE));
            deck.add(new Card(Card.Type.SKIP));
            deck.add(new Card(Card.Type.TACO_CAT));
            deck.add(new Card(Card.Type.HAIRY_POTATO_CAT));
            deck.add(new Card(Card.Type.CATTER_MELON));
            deck.add(new Card(Card.Type.BEARD_CAT));
            deck.add(new Card(Card.Type.RAINBOW_RALPHING_CAT));

        }
        for(int i = 0; i < 5; i++){
            deck.add(new Card(Card.Type.NOPE));
            deck.add(new Card(Card.Type.SEE_THE_FUTURE));
        }
        shuffle();
    }

    /** Getters and Setters**/
    public ArrayList<Card> getDeck() {
        return deck;
    }
    public HashMap<Integer, Card.Type> getAbrMap() {
        return abbreviationsMap;
    }

    /**
     * Create a deck of cards
     * @param deck an ArrayList of cards that will be added in the deck
     */
    public Deck(ArrayList<Card> deck){
        this.deck = deck;
    }

    /**
     * Gets and removes first card
     * @return top card of the deck
     */
    public Card getTopCard(){
        Card c = null;
        if(deck.size() == 0){
            System.out.println("Deck is empty");
        }
        else {
            c = deck.get(0);
            deck.remove(c);
        }
        return c;
    }

    /**
     * Insert a card at an index in the deck
     * @requires index between 0 and amount of cards in deck
     */
    public void putCardAtIndex(int index, Card card){
        deck.add(index, card);
    }

    /**
     * Picks a random card and removes it from the deck. (Not Exploding kittens)
     * Used to make the hand
     * @return a random card from the deck
     */
    public Card getRandomCard(){
        Random random = new Random();
        int r = random.nextInt(deck.size()-1);
        Card c = deck.get(r);

        while (c.getType().equals(Card.Type.EXPLODING_KITTEN)){
            r = random.nextInt(deck.size()-1);
            c = deck.get(r);
        }
        deck.remove(deck.get(r));
        return c;
    }

    /**
     * Shuffles the deck
     */
    public void shuffle() {
        Collections.shuffle(deck);
    }

    /**
     * Used for SEE_THE_FUTURE card
     */
    public void showTopThree(GameServer serv, Player p) throws IOException {
        if(deck.size() > 2) {
            serv.sendSingle("" + deck.get(0).getType(), p.getName());
            serv.sendSingle("" + deck.get(1).getType(), p.getName());
            serv.sendSingle("" + deck.get(2).getType(), p.getName());
        }
        else {
            for (int i = 0; i < deck.size(); i++) {
                System.out.println(deck.get(i).getType());
            }
        }
    }


    /**
     * Map that allocates a number to a card type
     */
    public void makeAbbreviationsMap(){
        abbreviationsMap.put(1, Card.Type.TACO_CAT);
        abbreviationsMap.put(2, Card.Type.HAIRY_POTATO_CAT);
        abbreviationsMap.put(3, Card.Type.CATTER_MELON);
        abbreviationsMap.put(4, Card.Type.BEARD_CAT);
        abbreviationsMap.put(5, Card.Type.RAINBOW_RALPHING_CAT);
        abbreviationsMap.put(6, Card.Type.DEFUSE);
        abbreviationsMap.put(7, Card.Type.SEE_THE_FUTURE);
        abbreviationsMap.put(8, Card.Type.SKIP);
        abbreviationsMap.put(9, Card.Type.SHUFFLE);
        abbreviationsMap.put(10, Card.Type.FAVOR);
        abbreviationsMap.put(11, Card.Type.NOPE);
        abbreviationsMap.put(12, Card.Type.ATTACK);
    }

}
