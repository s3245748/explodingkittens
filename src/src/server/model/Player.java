package server.model;

import exceptions.NoTurnException;
import server.controller.Console;

import java.io.IOException;
import java.util.ArrayList;
import server.controller.GameServer;

/**
 * Player class for Networked Game Exploding Kittens
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */

public class Player {
    private String name;
    private Hand hand;
    private boolean turn;
    private boolean outOfGame;
    private boolean nopeDraw;
    private boolean skipBool;
    private boolean attackBool;
    private boolean isAttacked;
    private Object lock = new Object();
    private int num;
    private boolean exp;
    private String victimName;
    private int indFavour;
    private int intSame;
    private String nopeAnswer;
    private boolean gaveCard;

    /**
     * Constructor for player
     * @param name the name of the player
     * @param hand the hand of the player
     * @param num the number of the player
     */
    public Player(String name, Hand hand, int num){
        this.name = name;
        this.turn = false;
        this.hand = hand;
        this.outOfGame = false;
        this.attackBool = false;
        this.nopeDraw = false;
        this.exp = false;
        this.num = num;

        this.victimName = "";
        this.indFavour = -1;
        this.intSame = -1;
        this.nopeAnswer = "";
        this.gaveCard = false;
    }

    /**
     * Getters and setters
     */
    public int getNum(){
        return num;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setExp(boolean exp){
        this.exp = exp;
    }
    public boolean getExp(){
        return exp;
    }
    public void setTurn(boolean turn){
        this.turn = turn;
    }
    public boolean getTurnBoolean(){
        return turn;
    }
    public Hand getHand(){
        return hand;
    }
    public void setHand(Hand hand){
        this.hand = hand;
    }
    public void setAttackBool(boolean attackBool){
        this.attackBool = attackBool;
    }
    public boolean getAttackBool(){
        return attackBool;
    }
    public void setIsAttacked(boolean bool){
        isAttacked = bool;
    }
    public boolean getIsAttacked(){
        return isAttacked;
    }


    public void setOutOfGame(boolean exploded){
        this.outOfGame = exploded;
    }
    public boolean getOutOfGame(){
        return outOfGame;
    }

    public boolean getNopeDraw(){
        return nopeDraw;
    }

    public void setSkipBool(boolean b){
        this.skipBool = b;
    }
    public boolean getSkipBool(){
        return skipBool;
    }


    /** Methods that wait for certain actions to happen */
    public synchronized void setGaveCard(boolean gaveCard){
        synchronized (lock) {
            this.gaveCard = gaveCard;
            lock.notifyAll();
        }
    }
    public synchronized void setNopeAns(String nopeAnswer){
        synchronized (lock) {
            this.nopeAnswer = nopeAnswer;
            System.out.println(nopeAnswer);
            lock.notifyAll();
        }
    }

    /**
     * Player has chosen a card to put down (as an index). Then get the type of card
     * Method calls corresponding player action methods depending on type of card
     * @param type the card type
     * @param players the list of players
     * @param deck the deck used
     * @param serv the server on which the game runs
     */
    public void putCard(Card.Type type, Deck deck, ArrayList<Player> players, GameServer serv) throws IOException, NoTurnException {
        switch (type) {
            case ATTACK: attack();
                break;
            case FAVOR: favor(players, serv);
                break;
            case NOPE: nope();
                break;
            case SHUFFLE: shuffle(deck);
                break;
            case SKIP: skip();
                break;
            case SEE_THE_FUTURE: seeTheFuture(deck, serv);
                break;
            default:
                break;
        }
    }

    /**
     * Gets the top Card from the deck (Card at index 0)
     * if it is an exploding kitten don't put it in the hand
     * @param deck for the deck
     * @return Card c at the top of the deck
     */
    public Card drawTopCard(Deck deck){
        Card c = deck.getTopCard();
        if(!c.getType().equals(Card.Type.EXPLODING_KITTEN)){
            hand.getHandCards().add(c);
        }
        if(c.getType().equals(Card.Type.NOPE)){
            nopeDraw = true;
        }
        return c;
    }

    /**
     * Plays a card and removes it from the hand
     * @param type the type of card that is played
     * @return the new hand with the removed card
     */
    public Card playCard(Card.Type type){
        return hand.removeCard(type);
    }

    /**
     * Draw the top card of the deck
     * @param draw the deck from which card is drawn
     * if exploding kitten, start actions to handle the exploding kitten card
     */
    public Card drawCard(Deck draw){
        Card c = drawTopCard(draw);
        if (c.getType().equals(Card.Type.EXPLODING_KITTEN)) {
            exp = true;
        }
        return c;
    }

    /**
     * Method to play the SEE_THE_FUTURE card
     * @param deck the deck that is used
     * @param serv the server that the game is running on
     * @throws IOException if an I/O error occurs
     */
    public void seeTheFuture(Deck deck, GameServer serv) throws IOException {
        deck.showTopThree(serv, this);
    }

    /**
     * Skips the turn of a player
     */
    public void skip(){
        setSkipBool(true);
    }

    /**
     * Choose cards to put down (a pair) and remove two times from hand
     * Choose player to steal card from
     * remove card from victim hand
     * add card to hand
     * @param players the list of players
     */
    public void twoSame(ArrayList<Player> players, GameServer serv) throws IOException, NoTurnException {
        getHand().makeMap();
        boolean has = getHand().returnMultiples()[0] > 0;
        serv.sendSingle(serv.doCombo(hand.getDoubleList(), 2, 0), name);
        hand.remove2(serv, this);
    }


    /**
     * choose cards to put down (a triple) and remove three times from hand
     * choose player to steal card from
     * show possible card and choose one
     * remove card from victim hand
     * add card to hand
     * @param deck the deck of cards that is used in the game
     * @param players list of players
     * @param serv the server the game is running on
     */
    public void threeSame(Deck deck, ArrayList<Player> players, GameServer serv) throws IOException, NoTurnException {
        getHand().makeMap();
        serv.sendSingle(serv.doCombo(hand.getTripleList(), 3, 0), name);
        hand.remove3(serv, this);
    }

    /**
     * Shuffles the deck
     * @param deck the deck that is used in the game
     */
    public void shuffle(Deck deck){
        deck.shuffle();
    }

    /**
     * Happens when a nope card is played
     */
    public void nope(){

    }

    /**
     * Choose a player to give a card to. Select a card to give
     * remove card from hand. Add card to other player
     * @param players the list of players
     * @param serv the server the game is running on
     * @throws IOException if an I/O error occurs
     */
    public void favor(ArrayList<Player> players, GameServer serv) throws IOException, NoTurnException {
        serv.sendSingle(serv.askFavour(), name);
        synchronized (lock) {
            while (!gaveCard) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            gaveCard = false;
        }

    }

    /**
     * Happens when an attack card is played
     */
    public void attack(){
        skipBool = true;
        attackBool = true;
    }

    /**
     * Asks a player if they want to use a nope card if they have one
     * @param turnP the turn of the current player
     * @param nopeHaver the player who has a nope card
     * @param tryMove the move the current player wants to do
     * @param serv the server the game is running on
     * @return true if the nope card is played, false otherwise
     * @throws IOException if an I/O error occurs
     */
    public boolean doNopeAsk(Player turnP, Player nopeHaver, String tryMove, GameServer serv) throws IOException {
        serv.sendSingle("You have a nope. " + "Player " + turnP.getName()  + tryMove + ". Do you want to nope this? (y,n)", nopeHaver.getName());
        serv.sendSingle(serv.requestNope(), nopeHaver.getName());
        synchronized (lock) {
            while (nopeAnswer.equals("")) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        String answer = nopeAnswer;
        nopeAnswer = "";
        if (answer.equals("y")){
            nopeHaver.getHand().removeCard(Card.Type.NOPE);
            return true;
        }
        return false;
    }
}
