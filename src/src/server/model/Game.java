package server.model;
import exceptions.NoTurnException;
import server.controller.GameServer;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Game class for Networked Game Exploding Kittens
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */

public class Game {
    private ArrayList<Player> players;
    private Deck draw;
    private int playerCount;
    private ArrayList<Player> nopeHavers;
    private int nopePutCount;
    private int attackCount;
    private boolean playedAttack;
    private boolean youPutNoAttack;
    private GameServer serv;
    private boolean turnMessSent;
    private int drawPlay;
    private String yesNo;
    private int deckPos;
    private boolean doneExplode;
    private String opt;
    private int chosenIn;
    private ArrayList<String> nameList;
    private Object lock = new Object();
    boolean newRound;


    /**
     * Constructor for Game
     * Asks how many players will play in this round and stores user input in var playerAmount.
     */
    public Game(int playerNum, ArrayList<Player> players, GameServer serv){
        this.turnMessSent = false;
        this.serv = serv;
        this.players = players;
        playerCount = playerNum;
        this.draw = new Deck(playerCount);
        makeHandsForPlayers();
        this.players = players;
        for (Player p: players) {
            System.out.println("Player: " + p.getName());
        }
        players.get(0).setTurn(true);
        this.playerCount = playerNum;
        this.nopeHavers = new ArrayList<>();
        nopePutCount = 0;
        makeNopeHavers();
        this.attackCount = 1;
        this.playedAttack = false;
        this.youPutNoAttack = false;

        //-----------Initialize for GameServer--------
        this.drawPlay = -5;
        this.yesNo = "";
        this.deckPos = -1;
        this.doneExplode = false;
        this.opt = "";
        this.chosenIn = -1;
        nameList();
        this.newRound = true;
    }

    /** Getters and setters */
    public Deck getDeck(){
        return draw;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }


    /**
     * Checks if the player is a computer player
     * @param p player that needs to be checked
     * @return true if the player is a computer player, false if not
     * @requires p != null
     */
    public boolean isComputer(Player p){
        return p.getName().equals("COMPUTER");
    }

    /**
     * Creates hands with cards for players
     */
    public void makeHandsForPlayers(){
        for (Player p : players){
            p.setHand(new Hand(draw));
        }
    }

    /**
     * Adds all names of players in an ArrayList
     */
    public void nameList(){
        ArrayList<String> nameList = new ArrayList<>();
        for (Player p: players){
            nameList.add(p.getName());
        }
        this.nameList = nameList;
    }

    /**
     * Starts the game
     * @throws IOException if an I/O error occurs
     */
    public void start() throws IOException, NoTurnException {
        boolean gameOver = false;
        while (!gameOver){
            Player p = getTurn();
            System.out.println("TURN OF " + p.getName());
            if (attackCount == 0){
                attackCount++;
            }
            turnMessSent = false;
            turn(p);
            if(!p.getOutOfGame()){
                nextPlayer();
            }
            else{
                p.setOutOfGame(false);
            }
            gameOver = checkGame();
        }
    }

    /**
     * Setter for newRound
     * @param round boolean to set if a new round starts.
     */
    public synchronized void setNewRound(boolean round){
        synchronized (lock) {
            this.newRound = round;
            lock.notifyAll();
        }
    }

    public synchronized void setDrawPlayS(int drawPlay){
        synchronized (lock) {
            this.drawPlay = drawPlay;
            lock.notifyAll();
        }
    }
    public synchronized void setDeckPos(int deckPos){
        synchronized (lock) {
            this.deckPos = deckPos;
            lock.notifyAll();
        }
    }
    public synchronized void setDoneExplode(boolean doneExplode){
        synchronized (lock) {
            this.doneExplode = doneExplode;
            lock.notifyAll();
        }
    }

    /**
     * Logic for the turn of one player
     * @param p Player whose turn it currently is
     */
    public void turn(Player p) throws IOException, NoTurnException {
        makeNopeHavers();
        Player curr = p;
        boolean turn = true;

        while (0 < attackCount && curr == p){
            turn = true;
            while(turn && !youPutNoAttack){
                youPutNoAttack = false;
                synchronized (lock) {
                    while (!newRound) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                newRound = false;
                if(!turnMessSent) {
                    serv.broadcastAllBut ("It is currently " + p.getName() + "'s turn.", p.getName());
                    turnMessSent = true;
                }
                serv.sendSingle("It is YOUR turn. This is your hand:", p.getName());
                if(!p.getHand().handEmpty()) {
                    if (isComputer(p)){
                        setDrawPlayS(-1);
                    }
                    else {
                        serv.sendSingle(serv.requestAction(p.getHand().getTypeHand(), nameList), p.getName());
                    }
                }
                else {
                    serv.sendSingle("Your hand is empty. You can only draw.", p.getName());
                    drawPlay = -1;
                }
                synchronized (lock) {
                    while (drawPlay == -5) {
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    int move = drawPlay;
                }
                int move = drawPlay;

                if (move == -1) {
                    serv.sendSingle("The card you drew is " + p.drawCard(draw).getType(), p.getName());
                    serv.broadcastAllBut("Player " + p.getName() + " drew a card", p.getName());

                    if (p.getExp()){
                        serv.broadcastAllBut("Player " + p.getName() + " drew an " + Card.Type.EXPLODING_KITTEN, p.getName());
                        isExplodingInGame(p);
                    }

                    if (p.getOutOfGame()) {
                        attackCount = 1;
                        nextPlayer();
                        nameList();
                        players.remove(p);
                    }
                    if (p.getNopeDraw()){
                        if(!nopeHavers.contains(p)) {
                            nopeHavers.add(p);
                        }
                    }
                    turn = false;
                    System.out.println("Turn set to false");
                    attackCount--;
                    drawPlay = -5;
                    setNewRound(true);
                }
                else if (move != -1) {
                    String amountMove = "s";
                    if (move == -2){
                        amountMove = "p";
                    }
                    if (move == -3){
                        amountMove = "t";
                    }
                    System.out.println("amount move " + amountMove);
                    Card.Type originalMove = null;
                    String tryMove = "";
                    if (amountMove.equals("p")) {
                        System.out.println("PAIR");
                        tryMove = " wanted to play two cards";
                        serv.sendSingle("You played two cards.\nWait in case someone wants to nope you.", p.getName());

                    }
                    if (amountMove.equals("t")) {
                        System.out.println("TRIPLE");
                        tryMove = " wanted to play three cards";
                        serv.sendSingle("You played three cards.\nWait in case someone wants to nope you.", p.getName());

                    }
                    if (amountMove.equals("s")) {
                        System.out.println("SINGLE");
                        originalMove = p.getHand().convertToType(move);
                        System.out.println("Played " + originalMove);
                        serv.broadcastAllBut ("Player " + p.getName() + " played a " + originalMove + " card.", p.getName());
                        serv.sendSingle("You played a " + originalMove + " card.\nWait in case someone wants to nope you.", p.getName());
                        setNewRound(true);
                        tryMove += " wanted to play " + originalMove;
                        p.playCard(originalMove);
                    }
                    makeNopeHavers();
                    boolean asking = true;
                    while (asking && !tryMove.equals(" wanted to play " + Card.Type.DEFUSE.toString()) && !tryMove.equals(Card.Type.EXPLODING_KITTEN.toString())) {
                        for (Player nopeP : nopeHavers) {
                            if (!nopeP.getName().equals(p.getName()) && !nopeP.getName().equals("COMPUTER")){
                                boolean nopeAsking = true;

                                nopeAsking = p.doNopeAsk(getTurn(), nopeP, tryMove, serv);

                                if (nopeAsking) {
                                    nopePutCount++;
                                    p.getHand().removeCard(Card.Type.NOPE);
                                    makeNopeHavers();
                                    tryMove += " noped by " + nopeP.getName();

                                    if (nopeHavers.isEmpty()) {
                                        asking = false;
                                    }
                                }
                            }
                        }
                        if(nopeHavers.contains(p)){
                            boolean nopeAsking = p.doNopeAsk(getTurn(), p, tryMove, serv);

                            if (nopeAsking) {
                                nopePutCount++;
                                p.getHand().removeCard(Card.Type.NOPE);
                                makeNopeHavers();
                                tryMove += " noped by " + p.getName();

                                if (nopeHavers.isEmpty()) {
                                    asking = false;
                                }
                            }
                        }
                        asking = false;
                    }
                    if (nopePutCount % 2 == 0) {
                        if (amountMove.equals("p")) {
                            p.twoSame(players, serv);
                            setNewRound(false);
                        } else if (amountMove.equals("t")) {
                            p.threeSame(draw, players, serv);
                            setNewRound(false);
                        } else if (originalMove != null) {
                            p.putCard(originalMove, draw, players, serv);
                        }
                        if (p.getAttackBool()) {
                            p.setAttackBool(false);
                            if (attackCount == 1){
                                attackCount = 2;
                            }
                            else {
                                attackCount += 2;
                            }
                            curr = whoseNext();
                            curr.setIsAttacked(true);
                            p.setIsAttacked(false);//----------
                        }
                        if (p.getSkipBool() && p.getIsAttacked()) {
                            if(attackCount > 1){
                                attackCount--;
                            }
                            else {
                                p.setIsAttacked(false);
                                p.setSkipBool(false);
                                turn = false;
                            }
                        }
                        else if(p.getSkipBool() && attackCount == 1){
                            attackCount--;
                            p.setSkipBool(false);
                            turn = false;
                        }
                        else if (p.getSkipBool()){
                            //attackCount--;
                            p.setSkipBool(false);
                            turn = false;
                        }
                    } else {
                        serv.broadcastAllBut("The card from " + p.getName() + " was noped! No action executed.", getTurn().getName());
                        serv.sendSingle("Your card was noped!" , getTurn().getName());
                    }
                    nopePutCount = 0;
                }
                drawPlay = -5;
            }
        }
    }

    /**
     * Sets the turn of the current player to false and the turn of the next player in the list to true
     * @requires getTurn of one player in players to be true
     * @ensures getTurn of next player in list to be true
     * @ensures getTurn of current getTurn to be false
     */
    public void nextPlayer(){
        for(int i = 0; i < players.size(); i++){
            int index = 0;
            if(players.get(i).getTurnBoolean()){
                index = (i + 1) % players.size();
                players.get(index).setTurn(true);
                players.get(i).setTurn(false);
                System.out.println("Player " + players.get(index).getName() + " turn NOW");
                break;
            }
        }
    }

    /**
     * Checks whose turn is next
     * @return index of the player whose turn it is next
     */
    public Player whoseNext(){
        for(int i = 0; i < players.size(); i++){
            int index = 0;
            if(players.get(i).getTurnBoolean()){
                index = (i + 1) % players.size();
                return players.get(index);
            }
        }
        return null;
    }

    /**
     * Checks whose turn it is
     * @return player whose turn it is
     */
    public Player getTurn(){
        for(Player p : players){
            if(p.getTurnBoolean()){
                return p;
            }
        }
        return null;
    }

    /**
     * Checks whether the game is over (if only one player is left)
     */
    public boolean checkGame() throws IOException {
        if (players.size() <= 1){
            serv.broadcastAll("Game over! You won!");
            return true;
        }
        return false;
    }


    /**
     * Go through playerList. If they have a nope, add to the nopeHavers list
     */
    public void makeNopeHavers(){
        nopeHavers = new ArrayList<>();
        for (Player p : players){
            if (p.getHand().hasNope() && !nopeHavers.contains(p)){
                nopeHavers.add(p);
            }
        }
    }

    /**
     * Handles player drawing an exploding kitten card
     * @param p player who draws the card
     * @throws IOException if an I/O error occurs
     */
    public void isExplodingInGame(Player p) throws IOException {
        if (p.getHand().hasType(Card.Type.DEFUSE)) {
            serv.sendSingle(serv.explodePlayer (false, draw.getDeck().size()), p.getName());
            p.getHand().removeCard(Card.Type.DEFUSE); //remove defuse card
            p.setOutOfGame(false); //player does not get kicked out of game
            p.setExp(false);
            synchronized (lock) {
                while (deckPos < 0) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            draw.putCardAtIndex(deckPos, new Card(Card.Type.EXPLODING_KITTEN)); //add card to index in deck
            serv.sendSingle("You set the EXPLODING_KITTEN at index " + deckPos + " in the deck", p.getName());
            deckPos = -1; // reset for next
            serv.broadcastAllBut("Player " + getTurn().getName() + " defused the " + Card.Type.EXPLODING_KITTEN, getTurn().getName());
        }
        else {
            serv.sendSingle(serv.explodePlayer (true, draw.getDeck().size()), p.getName());
            synchronized (lock) {
                while (!doneExplode) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            doneExplode = false;
            p.setOutOfGame(true);
        }
    }
}

