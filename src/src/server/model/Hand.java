package server.model;

import server.controller.GameServer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Hand class for Networked Game Exploding Kittens
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */

public class Hand {
    private ArrayList<Card> hand;
    private HashMap<Card.Type, Integer> cardTypeAmountCountMap;
    private ArrayList<Card.Type> doubleList;
    private ArrayList<Card.Type> triplesList;
    private Object lock = new Object();
    private String chosenCardString;


    /**Constructor
     * @ensures one DefuseCard to the Hand
     * @ensures add 7 other random cards to hand (no Exploding Kittens) (total of 8 cards)
     * @param deck the deck from which the cards are drawn
     */
    public Hand(Deck deck){
        hand = new ArrayList<>();
        cardTypeAmountCountMap = new HashMap<>();
        hand.add(new Card(Card.Type.DEFUSE));
        doubleList = new ArrayList<>();
        triplesList = new ArrayList<>();
        for(int i = 0; i < 7; i++){ // Only 7 times because the defuse card is already in
            Card c = deck.getRandomCard();
            hand.add(c);
        }
        chosenCardString = "";
    }

    /**
     * Getters and setters
     */
    public Hand(ArrayList<Card> hand){ //used for testing hand methods
        this.hand = hand;
    }

    public synchronized void setChosenCardString(String chosenCard){
        synchronized (lock) {
            this.chosenCardString = chosenCard;
            lock.notifyAll();
        }
    }

    public ArrayList<Card> getHandCards() {
        return hand;
    }

    public ArrayList<Card.Type> getDoubleList(){
        makeMap();
        return doubleList;
    }

    public ArrayList<Card.Type> getTripleList(){
        makeMap();
        return triplesList;
    }

    public boolean handEmpty(){
        return hand.isEmpty();
    }

    /**
     * Removes a card from the hand of type t (in case it exists in hand)
     * @param t type of the card that gets removed
     * @return the removed card. If card is not in hand, return null
     */
    public Card removeCard(Card.Type t){
        if(!handEmpty()) {
            for (Card c : hand) {
                if (c.getType().equals(t)) {
                    hand.remove(c);
                    return c;
                }
            }
        }
        return null;
    }

    /**
     * Creates a map cardTypeAmountCountMap that maps the type of card in the hand to the occurrences of this type (Type, int)
     * Calls the method makeDoubleAndTripleList()
     */
    public void makeMap(){
        cardTypeAmountCountMap = new HashMap<>();
        for (Card c : hand) {
            if (cardTypeAmountCountMap.get(c.getType()) == null){
                cardTypeAmountCountMap.put(c.getType(), 1);
            }
            else {
                cardTypeAmountCountMap.put(c.getType(), cardTypeAmountCountMap.get(c.getType()) +1 );
            }
        }
        makeDoubleAndTripleList();
    }

    /**
     * Make two lists: one for doubles and one for triples
     * double list: stores all card types in the hand that occur 2 or more times
     * triple list: stores all card types in the hand that occur 3 or more times
     */
    private void makeDoubleAndTripleList(){
        doubleList = new ArrayList<>();
        triplesList = new ArrayList<>();
        for (Card.Type t : cardTypeAmountCountMap.keySet()){
            if (cardTypeAmountCountMap.get(t) >= 2 && !doubleList.contains(t)){
                doubleList.add(t);
            }
            if (cardTypeAmountCountMap.get(t) >= 3 && !triplesList.contains(t)){
                triplesList.add(t);
            }
        }
    }

    /**
     * @return array of two values. First value for amount of doubles. Second for amount of triples
     */
    public int[] returnMultiples(){
        makeMap();
        return new int[] {doubleList.size(), triplesList.size()};
    }

    /**
     * @return a random card in the hand
     * (used for the double card move)
     */
    public Card retrunRandomCard(){
        Random random = new Random();
        int index = random.nextInt(hand.size());
        return hand.get(index);
    }

    /**
     * Check if the hand has a card of type t
     * @param t the type of card
     */
    public boolean hasType(Card.Type t){
        for (Card c : getHandCards()){
            if(c.getType().equals(t)) {
                return true;
            }
        }
        return false;
    }

    /**
     * add a card to the hand
     * @param c Card that gets added to hand
     * (used for double and triple card move and favor)
     */
    public void addCard(Card c){
        hand.add(c);
    }

    /**
     * Adds all card types in the hand of a player to an ArrayList
     * @return the ArrayList of card types in the players hand
     */
    public ArrayList<Card.Type> getTypeHand() {
        ArrayList<Card.Type> arrL = new ArrayList<>();
        for (Card c : hand){
            arrL.add(c.getType());
        }
        return arrL;
    }

    /**
     * Checks if a player has a nope card
     * @return true if the player has a nope card, false otherwise
     */
    public boolean hasNope(){
        for (Card c : hand){
            if (c.getType().equals(Card.Type.NOPE)){
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if a certain card has a certain type
     * @param s the type of card as a string
     * @return the type of card if it is the same as s, otherwise return null
     */
    public Card.Type getType(String s){
        for (Card c : hand){
            if (c.getType().toString().equals(s)){
                return c.getType();
            }
        }
        return null;
    }

    /**
     * Remove two cards from the hand of the type gotten by the double list and the index
     * @param serv for the server
     * @param p for the player
     */
    public void remove2(GameServer serv, Player p)  {
        synchronized (lock) {
            while (chosenCardString.equals("")) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        Card.Type type = getType(chosenCardString);
        chosenCardString = "";
        removeCard(type);
        removeCard(type);
    }

    /**
     * Remove three cards from the hand of the type gotten by the double list and the index
     * @param serv for server
     * @param p for player
     */
    public void remove3(GameServer serv, Player p)  {
        synchronized (lock) {
            while (chosenCardString.equals("")) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        Card.Type type = getType(chosenCardString);
        chosenCardString = "";
        removeCard(type);
        removeCard(type);
        removeCard(type);
    }


    /**
     * An index is inputted and the card type at that index in the hand is returned
     * @param index the index that is used to check which card is there
     */
    public Card.Type convertToType(int index){
        return hand.get(index-1).getType();
    }

}
