package server.controller;

import exceptions.ExitProgram;
import exceptions.NoTurnException;
import exceptions.NameAlreadyUsedException;
import protocol.ProtocolMessages;
import protocol.ServerProtocol;

import server.model.*;
import server.view.*;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import java.util.HashMap;
import java.util.HashSet;


/**
 * GameServer for the Networked Game Exploding Kittens
 * @author Christina Braun
 * @author Marit van der Valk
 */
public class GameServer implements Runnable, ServerProtocol {
    private ServerSocket ssock;
    private Object lock = new Object();
    private List<GameClientHandler> clients;
    private ArrayList<Player> playerListServer;
    private int next_client_no;
    private boolean allNamesUnique;
    private GameServerTUI view;
    private Console console;
    private static int PLAYER_AMOUNT;
    private int playerCount;
    private Game game;

    /**
     * Constructor for GameServer
     */
    public GameServer() {
        clients = new ArrayList<>();
        view = new GameServerTUI();
        next_client_no = 1;
        console = new Console();
        playerListServer = new ArrayList<>();
        this.allNamesUnique = false;
    }

    /**
     * Getter for the game
     * @return game
     */
    public Game getGame(){
        return game;
    }

    /**
     * Method for checking whose turn it is
     * @return player whose turn it is
     */
    public Player getTurn() throws NoTurnException {
        for(Player p : playerListServer){
            if(p.getTurnBoolean()){
                return p;
            }
        }
        throw new NoTurnException();
    }

    /**
     * Method for getting a player by name, excluding the player whose turn it is
     * @requires name != null
     * @ensures player with given name, excluding the current player, is returned, otherwise return null
     * @param name the name of the player to retrieve
     * @return the player with the specified name, excluding the current turn player
     */
    public Player getPlayerByNameNotCurr(String name) throws NoTurnException {
        printClientsAndPlayers();
        for (Player p : playerListServer) {
            if (p.getName().equals(name) && !getTurn().getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    /**
     * Method for getting a player by name
     * @requires name != null
     * @ensures player with given name is returned, otherwise return null
     * @param name the name of the player to retrieve
     * @return the player with the specified name
     */
    public Player getPlayerByName(String name){
        printClientsAndPlayers();
        for (Player p : playerListServer) {
            if (p.getName().equals(name)) {
                return p;
            }
        }
        return null;
    }

    /**
     * Method that will be executed when a new Thread is started
     * @ensures server thread is executed
     */
    public void run() {
        boolean openNewSocket = true;
        while (openNewSocket) {
            try {
                setup();
                boolean isTrue = true;
                while (isTrue) {
                    Socket sock = ssock.accept();
                    playerCount++;
                    String nameNum = "client " + String.format("%02d", next_client_no++);
                    view.showMessage("New client [" + nameNum + "] connected!");
                    GameClientHandler handler = new GameClientHandler(sock, this, nameNum, playerCount);
                    new Thread(handler).start();
                    clients.add(handler);
                    System.out.println(playerCount);
                    if (!(playerCount < PLAYER_AMOUNT)) {
                        isTrue = false;
                        System.out.println("End adding players " + playerCount + " " + PLAYER_AMOUNT);
                        openNewSocket = false;
                        System.out.println(clients.size());
                        startGameSetUp();
                    }
                }
            } catch (ExitProgram e1) {
                openNewSocket = false;
            } catch (IOException e) {
                System.out.println("A Client IO error occurred: "
                        + e.getMessage());
                if (!view.getBoolean("Do you want to open a new socket?")) {
                    openNewSocket = false;
                }
            } catch (NoTurnException e) {
                System.out.println(e);
            }
        }
        view.showMessage("See you later!");
    }

    /**
     * Sets up the server for accepting client connection
     * @throws ExitProgram if the user wants to exit
     * @ensures a server socket is set up for client connections
     */
    public void setup() throws ExitProgram {
        setupGame();
        ssock = null;
        while (ssock == null) {
            int port = view.getInt("Please enter the server port."); //8888
            try {
                view.showMessage("Attempting to open a socket at 127.0.0.1 "
                        + "on port " + port + "...");
                ssock = new ServerSocket(port, 0, InetAddress.getByName("127.0.0.1"));
                view.showMessage("Server started at port " + port);
            } catch (IOException e) {
                view.showMessage("ERROR: could not create a socket on "
                        + "127.0.0.1" + " and port " + port + ".");
                if (!view.getBoolean("Do you want to try again?")) {
                    throw new ExitProgram("User indicated to exit the "
                            + "program.");
                }
            }
        }
    }

    /**
     * Method for setting up a game, asking how many players will play the game
     * @ensures PLAYER_AMOUNT will be between 2 and 5, including 2 and 5
     */
    public void setupGame() {
        int playerNum = console.expectInt("How many players will play the game? (2 to 5)", 2, 5);
        PLAYER_AMOUNT = playerNum;
    }

    /**
     * Method for starting the game setup
     * @throws IOException if an I/O error occurs
     */
    public synchronized void startGameSetUp() throws IOException, NoTurnException {
        synchronized (lock) {
            while (!allNamesUnique) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        for (GameClientHandler h: clients){
            System.out.println("in c: " + h.getName() + " num: " + h.getNum());
        }
        for (Player p: playerListServer){
            System.out.println("in p: " + p.getName() + " num: " + p.getNum());
        }
        game = new Game(PLAYER_AMOUNT, playerListServer, this);
        game.start();
    }

    /**
     * Removes a clientHandler from the client list.
     * @requires client != null
     * @requires name != null
     */
    public void removeClient(String name) {
        for (int i = 0; i < playerListServer.size(); i++) {
            if (playerListServer.get(i).getName().equals(name)){
                clients.remove(i);
                break;
            }
        }
    }

    /**
     * Prints all clients and all players
     */
    public void printClientsAndPlayers(){
        for (GameClientHandler h: clients){
            System.out.println("in c: " + h.getName() + " num: " + h.getNum());
        }
        for (Player p: playerListServer){
            System.out.println("in p: " + p.getName() + " num: " + p.getNum());
        }
    }

    /**
     * Makes a player list to a string
     * @return string of a player
     */
    public String playersLToString() throws NoTurnException {
        String strP = "";
        for (Player pl : playerListServer){
            if(!pl.getName().equals(getTurn().getName())) {
                strP += pl.getName() + "-";
            }
        }
        return strP;
    }

    /**
     * Sends a message to only one player using the name of the player
     * @param message message to be sent
     * @param playerName player to which it should be sent
     * @throws IOException if an I/O error occurs
     */
    public void sendSingle(String message, String playerName) throws IOException {
        for (GameClientHandler h : clients){
            System.out.println("Client name: " + h.getName());
            if(h.getName().equals(playerName)){
                System.out.println("Sending: " + message);
                h.sendResponse(announceInPrivate(message));
            }
        }
    }

    /**
     * Method to send a message using the number of a player
     * @param message to message to be sent
     * @param handler the GameClientHandler to which it sends
     * @throws IOException if an I/O error occurs
     */
    public void sendSingleWNum(String message, GameClientHandler handler) throws IOException {
        handler.sendResponse(announceInPrivate(message));
    }

    /**
     * Method to broadcast an announcement to everyone but one player
     * @param message the message to be sent
     * @param playerName the name of the player which should be excluded
     * @throws IOException if an I/O error occurs
     */
    public void broadcastAllBut(String message, String playerName) throws IOException {
        System.out.println("Broadcasting to all but " + playerName + " :" + message);
        for (GameClientHandler h : clients){
            if(!h.getName().equals(playerName)){
                sendSingle(message, h.getName());
            }
        }
    }

    /**
     * Method to broadcast to every player
     * @param message message to be sent
     * @throws IOException if an I/O error occurs
     */
    public void broadcastAll(String message) throws IOException {
        System.out.println("Broadcasting: " + message);
        for (GameClientHandler h : clients){
            System.out.println("Client name: " + h.getName());
            if(h.getName() != null) {
                sendSingle(message, h.getName());
            }
        }
    }

    /**
     * Checks if a name is already used
     * @param potentialName the name the user wants to use
     * @param handler the GameClientHandler linked to the user
     * @throws IOException if an I/O error occurs
     */
    public void checkName(String potentialName, GameClientHandler handler) throws IOException, NameAlreadyUsedException {
        System.out.println(clients.size());
        System.out.println(playerListServer.size());
        for (GameClientHandler h: clients){
            if (h.getName().equals(potentialName)) {
                System.out.println("same name");
                sendSingleWNum(requestName(), handler);
                break;
                //throw new NameAlreadyUsedException("Name already used");
            }
            else {
                handler.setName(potentialName);
                playerListServer.add(new Player(potentialName, new Hand(new ArrayList<>()), handler.getNum()));
                break;
            }
        }
        if (handler.getName().equals("")){
            handler.setName(potentialName);
        }
        if (checkNameUniqueness()){
            synchronized (lock) {
                allNamesUnique = true;
                lock.notifyAll();
            }
        }
    }

    /**
     * Checks that all other names are unique
     * @return true if names are unique, false if not
     */
    public boolean checkNameUniqueness(){
        if (clients.size() == PLAYER_AMOUNT) {
            HashSet<String> set = new HashSet<>();
            for (GameClientHandler cl : clients) {
                System.out.println("get name " + cl.getName());
                if (!set.add(cl.getName())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Builds a string from a map
     * @param map the map that is used
     * @return a string from the map
     */
    public String buildStringFromMap(HashMap<Integer, Card.Type> map){
        String res = "";
        for (int ind : map.keySet()){
            res += map.get(ind) + "-";
        }
        return res;
    }

    // ------------------ Server Methods --------------------------

    /**
     * Method for playing a combo
     * contains information for asking player what card to put and from whom to get a card
     * @param combos a list of cards that are double or triple
     * @param amount the amount of cards the player has used in the combo
     * @param doCombo an integer for the combos
     * @return the protocol message from playing a combo
     */
    public String doCombo(ArrayList<Card.Type> combos, int amount, int doCombo) throws NoTurnException {
        String strP = playersLToString();
        String str = "";
        for (Card.Type t : combos){
            str += t + "-";
        }
        System.out.println(ProtocolMessages.DO_COMBO + ProtocolMessages.DELIMITER + str + ProtocolMessages.DELIMITER + amount + ProtocolMessages.DELIMITER + strP);
        return ProtocolMessages.DO_COMBO + ProtocolMessages.DELIMITER + str + ProtocolMessages.DELIMITER + amount + ProtocolMessages.DELIMITER + strP;
    }

    /**
     * Method for the askFavour card.
     * contains information for asking player who he/she wants a card from
     * @return the protocol message for the askFavour card
     */
    public String askFavour() throws NoTurnException {
        return ProtocolMessages.ASK_FAVOUR + ProtocolMessages.DELIMITER + playersLToString();
    }

    /**
     * Method for playing a favour card.
     * contains information for asking victim what card to give
     * @param playableCards cards that can be played
     * @param victimName the name of the player it is aimed at
     * @return the protocol message for requesting a favor
     */
    @Override
    public String requestFavor(List<Card.Type> playableCards, String victimName) {
        String str = "";
        for (Card.Type t : playableCards){
            str += t + "-";
        }
        return ProtocolMessages.REQUEST_FAVOUR + ProtocolMessages.DELIMITER + str + ProtocolMessages.DELIMITER + getGame().getTurn().getName() + ProtocolMessages.DELIMITER + victimName;
    }

    /**
     * Method for requesting an action from a player
     * @param playableCards refers to the playable cards in a players hand
     * @param alivePlayers string refers to player's name
     * @return the protocol message for requesting an action
     */
    @Override
    public String requestAction(ArrayList<Card.Type> playableCards, ArrayList<String> alivePlayers) {
        String str = "";
        for (Card.Type t : playableCards){
            str += t + "-";
        }
        System.out.println("Sending " + ProtocolMessages.REQUEST_ACTION + ProtocolMessages.DELIMITER + str + ProtocolMessages.DELIMITER);
        return ProtocolMessages.REQUEST_ACTION + ProtocolMessages.DELIMITER + str + ProtocolMessages.DELIMITER;
    }

    /**
     * Method for handling a combo of three
     * contains information for asking the player what card he desires
     * @param vicName the name of the player the combo is aimed at
     * @return the protocol message for handling a combo
     */
    @Override
    public String handlePlayCombo(String vicName) {
        String mapString = buildStringFromMap(getGame().getDeck().getAbrMap());
        return ProtocolMessages.HANDLE_PLAY_COMBO + ProtocolMessages.DELIMITER + vicName + ProtocolMessages.DELIMITER + mapString;
    }

    /**
     * Method for if a player drew an exploding kitten
     * @param isExploded refers to if the player has exploded or not. True means they exploded. False means they had a defuse card.
     * @param deckSize refers to the size of the deck
     * @return the protocol message for exploding a player
     */
    @Override
    public String explodePlayer(boolean isExploded, int deckSize) {
        return ProtocolMessages.EXPLODE_PLAYER + ProtocolMessages.DELIMITER + isExploded + ProtocolMessages.DELIMITER + deckSize + ProtocolMessages.DELIMITER;
    }

    /**
     * Method for requesting a nope card
     * @return protocol message for requesting a nope card
     */
    @Override
    public String requestNope() {
        return ProtocolMessages.REQUEST_NOPE + ProtocolMessages.DELIMITER;
    }

    /**
     * Method for requesting a name
     * @return the protocol message for requesting a name
     */
    @Override
    public String requestName() {
        return ProtocolMessages.REQUEST_NAME + ProtocolMessages.DELIMITER;
    }

    /**
     * Method to send an announcement to a player
     * @param message message contents
     * @return the protocol message to send an announcement to a player
     */
    @Override
    public String announceInPrivate(String message) {
        return ProtocolMessages.ANNOUNCE_IN_PRIVATE + ProtocolMessages.DELIMITER + message;
    }

    @Override
    public String handleRequestNope() {
        return null;
    }

    @Override
    public String askReplayGame() {
        return null;
    }

    @Override
    public String handleReplayResponse(boolean isNewGame) {
        return null;
    }

    @Override
    public String handleStartGame(ArrayList<String> turnOrder, ArrayList<Card.Type> hand) {
        return null;
    }
    @Override
    public String handleLeaveGame() {
        return null;
    }
    @Override
    public String handleInsert() {
        return null;
    }
    @Override
    public String handlePlayCard() {
        return null;
    }

    @Override
    public String showDeckSize(int deckSize, int numOfExploding) {
        return null;
    }

    @Override
    public String handleEndTurn(Card.Type cardType) {
        return null;
    }

    @Override
    public String handleSelectName() {
        return null;
    }
    @Override
    public String announceInGeneral(String message){
        return null;
    }

    @Override
    public String displayErrorCode(String code, String message) {
        return null;
    }

    @Override
    public String handleRequestFavor() {
        return null;
    }

    @Override
    public String displayStatusCode(String code, String message) {
        return null;
    }

    // ------------------ Main --------------------------

    /** Start a new GameServer */
    public static void main(String[] args) {
        GameServer server = new GameServer();
        System.out.println("Welcome to the Game Server! Starting...");
        new Thread(server).start();
    }
}

