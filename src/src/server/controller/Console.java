package server.controller;

import server.model.Hand;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

/**
 * Console for Networked Game Exploding Kittens
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */
public class Console {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private PrintStream out = System.out;

    /**
     * Method that flushes the input stream so that it ignores the previous input.
     * (For example, if a player inputs something when it is not their turn, it is ignored).
     */
    private void flushInputStream() {
        try {
            while (System.in.available() > 0) {
                System.in.read();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method that checks if the user inputted a string.
     * @param question question that is asked to the client
     * @return a text of type string
     */
    public String readString(String question){
        flushInputStream();
        out.println(question);
        String answer = null;
        try {
            answer = reader.readLine();
        }
        catch (IOException e){
            out.println("Error reading.");
            e.printStackTrace();
        }
        if (answer == null){
            return "";
        }
        return answer;
    }

    /**
     * Method that checks if the user inputted an integer. Will ask again if value is not an integer.
     * @param question question that is asked to the client
     * @return a text of type integer
     */
    public int readInt(String question){
        flushInputStream();
        int num = 0;
        while (true){
            try {
                num = Integer.parseInt(readString(question));
                return num;
            }
            catch (NumberFormatException e){
                out.println("Not an integer.");
            }
        }
    }

    /**
     * Method that checks that the user inputs a valid answer. (Example to draw or put a card the input must be either "d" or "p")
     * @param question text for the user to know what to input
     * @param expectVal array of possible answers. If given answer is not in the array ask again
     * @return a String that is in the ArrayList of expectVal
     */
    public String expectString(String question, String[] expectVal, String error){
        String ans = "";
        System.out.println(question);
        while (true){
            ans = readString("");
            for (String s : expectVal){
                if (s.equals(ans)){
                    return ans;
                }
            }
            System.out.println(error);
        }
    }

    /**
     * Method that checks that the user inputs a valid answer.
     * @param question text for the user to know what to input
     * @param min minimum integer that can be inputted, including this integer
     * @param max maximum integer that can be inputted, including this integer
     * @return an integer that is in between the minimum and maximum
     */
    public int expectInt(String question, int min, int max){
        int ans = 0;
        while (true){
            ans = readInt(question);
            if (ans <= max && ans >= min){
                return ans;
            }
            System.out.println("range must be between: " + min + " and " + max);
        }
    }
}