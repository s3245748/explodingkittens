package server.controller;

import exceptions.NoTurnException;
import protocol.ProtocolMessages;
import server.model.Player;

import java.io.*;
import java.net.Socket;

import java.util.ArrayList;
import static protocol.ProtocolMessages.*;
import server.model.*;
import exceptions.NameAlreadyUsedException;


/**
 * GameClientHandler for the Networked Game Exploding Kittens.
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */
public class GameClientHandler implements Runnable {
    private BufferedReader in;
    private BufferedWriter out;
    private Socket sock;
    private GameServer srv;
    private String nameNum;
    private String name;
    private int num;

    /**
     * Constructor for the GameClientHandler
     * @param sock the socket the connection should be made on
     * @param srv the server the connection should be made to
     * @param nameNum the number of the client connected, as a string
     * @param num the number of the client connected, as an integer
     * @throws IOException if an I/O error occurs
     */
    public GameClientHandler(Socket sock, GameServer srv, String nameNum, int num) throws IOException {
        try {
            in = new BufferedReader( new InputStreamReader(sock.getInputStream()));
            out = new BufferedWriter( new OutputStreamWriter(sock.getOutputStream()));
            this.sock = sock;
            this.srv = srv;
            this.nameNum = nameNum;
            this.name = "";
            this.num = num;
        } catch (IOException e) {
            shutdown();
        }
    }

    /**
     * Getter for the name
     * @return the name of the client
     */
    public String getName(){
        return name;
    }

    /**
     * Setter for the name
     * @param name the name it should be set to
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Getter for the number of the client
     * @return the number of the client
     */
    public int getNum(){
        return num;
    }

    /**
     * Get the current turn of a player
     * @return the player whose turn it is
     */
    public Player getCurr() throws NoTurnException {
        return srv.getTurn();
    }

    /**
     * Get the current hand of the player
     * @return the current hand of the player
     */
    public Hand getCurrHand() throws NoTurnException {
        return getCurr().getHand();
    }

    /**
     * Method to send a response to a client
     * @param response the text to be sent
     * @throws IOException if an I/O error occurs
     */
    public void sendResponse(String response) throws IOException {
        System.out.println("Response: " + response);
        out.write(response);
        out.newLine();
        out.flush();
    }

    /**
     * Continuously listens for incoming messages from a client and handles the command
     */
    public void run() {
        String msg;
        try {
            msg = in.readLine();
            while (msg != null) {
                System.out.println("> [" + nameNum + "] Incoming: " + msg);
                handleCommand(msg);
                out.newLine();
                out.flush();
                msg = in.readLine();
            }
            shutdown();
        } catch (IOException e) {
            shutdown();
        } catch (NameAlreadyUsedException e) {
            System.out.println(e);
        } catch (NoTurnException e) {
            System.out.println(e);;
        }
    }

    /**
     * Handles the incoming command from a client
     * @param msg the text from the client
     * @throws IOException if an I/O error occurs
     * @throws NameAlreadyUsedException if a player name is already used
     */
    private void handleCommand(String msg) throws IOException, NameAlreadyUsedException, NoTurnException {
        String[] sptMsg = msg.split(ProtocolMessages.DELIMITER);
        if(msg.contains(ProtocolMessages.DELIMITER)){
        }
        else {
            System.out.println(msg);
        }
        switch (sptMsg[0]) {
            case HANDLE_PLAY_CARD:
                if (sptMsg[1].equals("-2")) {
                    if (srv.getGame().getTurn().getHand().returnMultiples()[0] == 0) {
                        srv.sendSingle("You don't have any pairs", srv.getGame().getTurn().getName());
                        srv.sendSingle(srv.requestAction(srv.getGame().getTurn().getHand().getTypeHand(), new ArrayList<>()), srv.getGame().getTurn().getName());
                    }
                }
                if (sptMsg[1].equals("-3")){
                    System.out.println("In -3");
                    if (srv.getGame().getTurn().getHand().returnMultiples()[1] == 0){
                        srv.sendSingle("You don't have any triples", srv.getGame().getTurn().getName());
                        srv.sendSingle(srv.requestAction(srv.getGame().getTurn().getHand().getTypeHand(), new ArrayList<>()), srv.getGame().getTurn().getName());
                    }
                }
                srv.getGame().setDrawPlayS(Integer.parseInt(sptMsg[1]));
                break;
            case CHOOSE_FAVOUR:
                Player victim = srv.getPlayerByNameNotCurr(sptMsg[1]);
                srv.sendSingle(srv.getGame().getTurn().getName() + " gets a card from you.", sptMsg[1]);
                srv.sendSingle(srv.requestFavor(victim.getHand().getTypeHand(), victim.getName()), sptMsg[1]);
                break;
            case HANDLE_REQUEST_FAVOUR:
                System.out.println(msg);
                System.out.println(sptMsg[2]);
                Player pl = srv.getPlayerByName(sptMsg[2]);
                System.out.println(" name " + pl.getName());

                Card.Type ty = pl.getHand().getTypeHand().get(Integer.parseInt(sptMsg[1])-1);
                System.out.println(ty);
                pl.getHand().removeCard(ty);
                getCurrHand().addCard(new Card(ty));
                srv.sendSingle("Player " + pl.getName() + " gave you a " + ty + " card.", getCurr().getName());
                srv.broadcastAllBut("Player " + pl.getName() + " gave " + getCurr().getName() + " a " + ty + " card.", getCurr().getName());
                getCurr().setGaveCard(true);
                srv.getGame().setNewRound(true);
                break;
            case PLAY_COMBO:
                System.out.println(msg);
                if (Integer.parseInt(sptMsg[1]) == 2) {
                    Player curr = srv.getGame().getTurn();
                    Hand currHand = curr.getHand();
                    currHand.removeCard(currHand.getType(sptMsg[3]));
                    currHand.removeCard(currHand.getType(sptMsg[3]));

                    srv.getGame().getTurn().getHand().setChosenCardString(sptMsg[2]);
                    Player victimCombo2 = srv.getPlayerByNameNotCurr(sptMsg[2]);
                    Card c = victimCombo2.getHand().retrunRandomCard();
                    srv.sendSingle("You got a " + c.getType() + " card from " + victimCombo2.getName(), srv.getGame().getTurn().getName());
                    srv.sendSingle("Player " + srv.getGame().getTurn().getName() + " got a " + c.getType() + " card from you." , victimCombo2.getName());
                    victimCombo2.getHand().removeCard(c.getType());
                    srv.getTurn().getHand().addCard(c);
                    srv.getGame().setNewRound(true);
                }
                else {
                    Player curr = srv.getGame().getTurn();
                    Hand currHand = curr.getHand();
                    currHand.removeCard(currHand.getType(sptMsg[3]));
                    currHand.removeCard(currHand.getType(sptMsg[3]));
                    currHand.removeCard(currHand.getType(sptMsg[3]));

                    currHand.setChosenCardString(sptMsg[2]);
                    Player victimCombo3 = srv.getPlayerByNameNotCurr(sptMsg[2]);

                    srv.sendSingle("What card would you like from " + victimCombo3.getName() + "?", curr.getName());
                    srv.sendSingle(srv.handlePlayCombo(victimCombo3.getName()), curr.getName());
                }
                break;
            case DO_EXPLODE:
                System.out.println( " name : " + srv.getGame().getTurn().getName());
                String explodedName = srv.getGame().getTurn().getName();
                srv.printClientsAndPlayers();
                srv.removeClient(explodedName);

                srv.printClientsAndPlayers();

                srv.broadcastAll("Player " + explodedName + " exploded.");
                srv.getGame().setDoneExplode(true);
                srv.getGame().setNewRound(true);
                break;
            case INSERT_EXPLODE:
                srv.getGame().setDeckPos(Integer.parseInt(sptMsg[1]));
                break;
            case HANDLE_REQUEST_NOPE:
                System.out.println(msg);
                System.out.println(sptMsg[1]);
                String nopeVal = "n";
                if (sptMsg[1].equals("true")){
                    nopeVal = "y";
                }
                getCurr().setNopeAns(nopeVal);
                srv.getGame().setNewRound(true);
                break;
            case SELECT_NAME:
                srv.checkName(sptMsg[1], this);
                break;
            case EXECUTE_COMBO3:
                Player p = srv.getPlayerByNameNotCurr(sptMsg[1]);
                System.out.println("got " + p.getName());
                Card.Type type = p.getHand().getType(sptMsg[2]);
                if (p.getHand().hasType(type)){
                    p.getHand().removeCard(type);
                    srv.getTurn().getHand().addCard((new Card(type)));
                    srv.broadcastAllBut("Player " + p.getName() + " has to give a " + type + " to " + srv.getTurn().getName(), getCurr().getName());
                    srv.sendSingle("You got a " + type + " from " + p.getName(), getCurr().getName());
                }
                else {
                    srv.broadcastAll("Player " + p.getName() + " does not have a card of type " + type);
                }
                srv.getGame().setNewRound(true);
                break;
            default:
                break;
        }
    }

    /**
     * Shut down the connection to this GameServer by closing the socket and
     * the In- and OutputStreams.
     */
    private void shutdown() {
        System.out.println("> [" + nameNum + "] Shutting down.");
        try {
            in.close();
            out.close();
            sock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        srv.removeClient(this.getName());
    }
}

