package server.view;

import java.io.PrintWriter;
import java.util.Scanner;


/**
 * GameServerTUI for Networked Game Exploding Kittens
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */
public class GameServerTUI implements GameServerView {

    private PrintWriter console;

    public GameServerTUI() {
        console = new PrintWriter(System.out, true);
    }

    @Override
    public void showMessage(String message) {
        console.println(message);
    }

    @Override
    public String getString(String question) {
        Scanner scanner = new Scanner(System.in);
        console.println(question);
        return scanner.nextLine();
    }

    @Override
    public int getInt(String question) {
        Scanner scanner = new Scanner(System.in);
        console.println(question);
        return scanner.nextInt();
    }

    @Override
    public boolean getBoolean(String question) {
        Scanner scanner = new Scanner(System.in);
        String response = scanner.nextLine();
        while (!response.equals("y") && !response.equals("n")){
            System.out.println(question);
            response = scanner.nextLine();
        }
        return response.equals("y");
    }

}
