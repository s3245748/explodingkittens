package protocol;

/**
 * Protocol for Networked Game Exploding Kittens.
 *
 * @author Christina Braun
 * @author Marit van der Valk
 *
 */

public class ProtocolMessages {

    public static final String DELIMITER = ";";
    public static final String DELIMITER_2 = "-";
    public static final String DELIMITER_3 = "#";

    public static final String EOT = "--EOT--";
    public static final String HELLO = "h";
    public static final String NAME = "n";
    public static final String EXIT = "e";

    // Selecting an action
    public static final String REQUEST_ACTION = "RA";
    public static final String PLAY_CARD = "PC";
    public static final String HANDLE_PLAY_CARD = "HPC";
    public static final String CHECK_DECK_SIZE = "CDS";
    public static final String SHOW_DECK_SIZE = "SDS";
    public static final String END_TURN = "ET";
    public static final String HANDLE_END_TURN = "HET";
    public static final String PLAY_COMBO = "PCO";
    public static final String HANDLE_PLAY_COMBO = "HPCO";


    // Exploding kitten is drawn
    public static final String EXPLODE_PLAYER = "EX";
    public static final String DO_EXPLODE = "DX";
    public static final String INSERT_EXPLODE = "IE";
    public static final String HANDLE_INSERT = "HI";

    // Nope card
    public static final String REQUEST_NOPE = "RN";
    public static final String REQUEST_NOPE_RESPONSE = "RNR";
    public static final String HANDLE_REQUEST_NOPE = "HRN";

    // Starting or leaving game
    public static final String START_GAME = "SG";
    public static final String HANDLE_START_GAME = "HSG";
    public static final String LEAVE_GAME = "LG";
    public static final String HANDLE_LEAVE_GAME = "HLG";

    // Selecting name for player
    public static final String REQUEST_NAME = "RNA";
    public static final String SELECT_NAME = "SN";
    public static final String HANDLE_SELECT_NAME = "HSN";

    // Announcements
    public static final String ANNOUNCE_IN_GENERAL = "AIG";
    public static final String DO_GENERAL_ANNOUNCE = "DGA";
    public static final String ANNOUNCE_IN_PRIVATE = "AIP";
    public static final String DO_PRIVATE_ANNOUNCEMENT = "DPA";
    public static final String DISPLAY_ERROR_CODE = "DEC";
    public static final String DO_ERROR_DISPLAY = "DED";
    public static final String DISPLAY_STATUS_CODE = "DSC";
    public static final String DO_STATUS_DISPLAY = "DSD";
    public static final String REQUEST_FAVOUR = "RF";
    public static final String CHOOSE_FAVOUR = "CF";
    public static final String HANDLE_REQUEST_FAVOUR = "HRF";

    // Own protocol messages
    public static final String DO_COMBO = "DC";
    public static final String EXECUTE_COMBO3 = "EC";
    public static final String ASK_FAVOUR = "AF";

}
