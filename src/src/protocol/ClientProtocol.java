package protocol;

/**
 * Defines the methods that the GameClient should support.
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */
public interface ClientProtocol {

    /**
     * A player may decide to play a card from their playable cards,
     * in which case they return the index that the card is located
     * in their hand and optionally the player that the card affects
     * which is -1 if not needed. All players are updated
     * (see announcements)
     * After which the GameServer does RequestAction again.
     * @param cardIndex index of where the card is in the players hand
     * @param playerIndex index of the player in the player list
     * @return the protocol message of the played card from this client
     */
    public String playCard(int cardIndex, int playerIndex);

    /**
     * A player may decide to check the amount of cards in
     * the deck, and the current exploding kittens in the deck.
     * @return the size of the deck as a String
     */
    public String checkDeckSize();

    /**
     * Means drawing a card
     * The player indicates to the GameServer
     * that they decide to end their turn.
     */
    public String endTurn();

    /**
     * Players are able to select either 2 or 3 cards which
     * perform the combo actions. The targeted player also
     * needs to be selected. After which the GameServer does
     * RequestAction again.
     * @param amount amount of cards used in the combo
     * @param victimName the player to which the combo will be pointed at
     * @param cardType the type of card used in the combo
     */
    public String playCombo(int amount, String victimName, String cardType);

    /** Method for handling an exploding kitten
     * @return message for when a player draws an exploding kitten
     */
    public String doExplode();

    /** Method for inserting an exploding kitten in the deck
     * @param deckIndex index of the place in the deck where the card should be inserted
     * @return message for inserting an exploding kitten in the deck
     */
    public String insertExplode(int deckIndex);

    /** Method for choosing to whom the favour card is aimed at
     * @param playerName name of the player whom the favour card is aimed at
     * @return message for choosing the player for the favour card
     */
    public String chooseFavor(String playerName);

    /** Method for handling the favour card
     * @param chosenCard the card that was chosen for the favour
     * @param fromName name of the player
     * @return message for handling a favour card
     */
    public String handleRequestFavor(int chosenCard, String fromName);

    /** Method for requesting a nope card
     * @param isUsed boolean for whether the nope card was played or not
     * @return message for handling requesting a nope card
     */
    public String requestNopeResponse(boolean isUsed);

    /**
     * Tells the GameServer what name was decided.
     * Names may not be the same between two players.
     * @param name name of the client
     */
    public String selectName(String name);

}
