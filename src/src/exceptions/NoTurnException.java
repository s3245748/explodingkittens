package exceptions;

public class NoTurnException extends Exception{

    public NoTurnException(String msg) {
        super(msg);
    }
    public NoTurnException() {
        super("No player found with turn");
    }

}
