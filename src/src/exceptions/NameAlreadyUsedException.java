package exceptions;

public class NameAlreadyUsedException extends Exception{
    public NameAlreadyUsedException(String msg) {
        super(msg);
    }
    public NameAlreadyUsedException() {
        super("Name already taken");
    }


}