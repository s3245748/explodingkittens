package client.controller;

import exceptions.ServerUnavailableException;
import client.view.ComputerClientTUI;

import java.io.*;

/**
 * ComputerClient for Networked Game Exploding Kittens.
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */

public class ComputerClient extends GameClient{
    private ComputerClientTUI computerTUI;
    private BufferedReader in;
    private BufferedWriter out;
    private String name;

    public ComputerClient() {
        super();
        this.computerTUI = new ComputerClientTUI(this);
        try {
            start();
        }catch(Exception e){
            System.out.println("Error ComputerClient constructor: " + e);
        }
    }
    @Override
    public void start() {
        try {
            askName();
            createConnection();
            this.in = getIn();
            this.out = getOut();
            handleStart();
            startServerListener();
            computerTUI.start();
        } catch(Exception e){
            computerTUI.showMessage("Error ComputerClient start: " + e);
        }
    }
    @Override
    public void startServerListener(){
        ServerListenerThread listener = new ServerListenerThread(in, computerTUI);
        listener.start();
    }

    @Override
    public void handleStart() throws ServerUnavailableException {
        sendMessage(selectName(name));
        computerTUI.showMessage("Welcome to the the Exploding Kittens Game: \nWait for enough players to connect...");
    }

    public static void main(String[] args) {
        (new ComputerClient()).start();
    }

}
