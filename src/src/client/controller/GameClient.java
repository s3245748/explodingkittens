package client.controller;

import client.view.GameClientTUI;
import exceptions.ExitProgram;
import exceptions.ServerUnavailableException;
import protocol.ClientProtocol;
import protocol.ProtocolMessages;
import server.controller.Console;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * GameClient for Networked Game Exploding Kittens.
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */
public class GameClient implements ClientProtocol {

    private Socket serverSock;
    private BufferedReader in;
    private BufferedWriter out;
    private GameClientTUI gameTUI;
    private Console console;
    private String name;

    /**
     * Constructs a new GameClient. Initialises the GameTUI and the Console
     */
    public GameClient() {
        this.console = new Console();
        this.gameTUI = new GameClientTUI(this);
        try {
            start();
        }catch(Exception e){
            System.out.println("Error GameClient constructor: " + e);
        }
    }

    /**
     * Getters and setters
     */
    public BufferedReader getIn(){
        return in;
    }
    public BufferedWriter getOut(){
        return out;
    }
    public String getName(){
        return this.name;
    }

    /**
     * Starts the process of setting up a GameClient
     *
     */
    public void start() {
        try {
            askName();
            createConnection();
            handleStart();
            startServerListener();
            gameTUI.start();
        } catch(Exception e){
            gameTUI.showMessage("Error GameClient start: " + e);
        }
    }

    /**
     * Creates a connection to the GameServer.
     *
     * @throws ExitProgram if a connection is not established and the user
     * 				       indicates to want to exit the program.
     * @ensures serverSock contains a valid socket connection to a GameServer
     */
    public void createConnection() throws ExitProgram {
        clearConnection();
        while (serverSock == null) {
            String host = "127.0.0.1";
            int port = 8888;
            try {
                InetAddress addr = InetAddress.getByName(host);
                System.out.println("Attempting to connect to " + addr + ":" + port + "...");
                serverSock = new Socket(addr, port);
                in = new BufferedReader(new InputStreamReader(
                        serverSock.getInputStream()));
                out = new BufferedWriter(new OutputStreamWriter(
                        serverSock.getOutputStream()));
            } catch (IOException e) {
                System.out.println("ERROR: could not create a socket on "
                        + host + " and port " + port + ".");
                if(false) {
                    throw new ExitProgram("User indicated to exit.");
                }
            }
        }
    }

    /**
     * Resets the serverSock and In- and OutputStreams to null.
     */
    public void clearConnection() {
        serverSock = null;
        in = null;
        out = null;
    }

    /**
     * Sends a message to the connected GameServer, followed by a new line.
     * The stream is then flushed.
     *
     * @param msg the message to write to the OutputStream.
     * @throws ServerUnavailableException if IO errors occur.
     */
    public synchronized void sendMessage(String msg) throws ServerUnavailableException {
        if (out != null) {
            try {
                out.write(msg);
                out.newLine();
                out.flush();
            } catch (IOException e) {
                System.out.println(e.getMessage());
                throw new ServerUnavailableException("Could not write to GameServer.");
            }
        } else {
            throw new ServerUnavailableException("Could not write to GameServer.");
        }
    }

    /**
     * Closes the connection by closing the In- and OutputStreams, as
     * well as the Socket.
     */
    public void closeConnection() {
        System.out.println("Closing the connection...");
        try {
            in.close();
            out.close();
            serverSock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Starts a new ServerListenerThread.
     */
    public void startServerListener(){
        ServerListenerThread listener = new ServerListenerThread(in, gameTUI);
        listener.start();
    }

    /**
     * Handles the start of the client connecting. Asks for a name input.
     * @throws ServerUnavailableException
     */
    public void handleStart() throws ServerUnavailableException {
        sendMessage(selectName(name));
        gameTUI.showMessage("Welcome to the the Exploding Kittens Game: \nWait for enough players to connect...");
    }

    /** Method for executing a combo with 3 cards
     * @param name name of the player from which a card could be removed (victim)
     * @param type the type of card used in the combo as a string
     * @return protocol message for executing a combo with 3 cards
     */
    public String executeCombo3(String name, String type){
        return ProtocolMessages.EXECUTE_COMBO3 + ProtocolMessages.DELIMITER + name + ProtocolMessages.DELIMITER + type;
    }

    /**
     * Method for asking the name of a client
     */
    public void askName() {
        this.name  = console.readString("What is your name?");
    }

    /** Method for exiting the game and closing the connection
     * @throws ServerUnavailableException if closing the connection does not work
     */
    public void sendExit() throws ServerUnavailableException {
        sendMessage(String.valueOf(ProtocolMessages.EXIT));
        closeConnection();
    }


    //-----------------------Client methods----------------------------

    /**
     * Method for a client to play a certain card.
     * @param cardIndex index of the card in the hand of the player. (if -1: draw, -2 combo of 2, -3 combo of 3)
     * @param playerIndex index of the player in the player list (Not always used)
     * @return the protocol message of the played card from this client
     */
    @Override
    public String playCard(int cardIndex, int playerIndex) {
        return ProtocolMessages.HANDLE_PLAY_CARD + ProtocolMessages.DELIMITER + cardIndex + ProtocolMessages.DELIMITER + playerIndex;
    }

    @Override
    public String checkDeckSize() {
        return null;
    }

    @Override
    public String endTurn() {
        return null;
    }

    /** Method for a client to play a combo
     * @requires amount == 2 or amount == 3
     * @param amount the amount of cards used in the combo
     * @param victimName the player to which the combo will be pointed at
     * @param cardType the type of card used in the combo
     * @return the protocol message for the played combo
     */
    @Override
    public String playCombo(int amount, String victimName, String cardType) {
        return ProtocolMessages.PLAY_COMBO + ProtocolMessages.DELIMITER + amount + ProtocolMessages.DELIMITER + victimName + ProtocolMessages.DELIMITER + cardType + ProtocolMessages.DELIMITER;
    }

    /** Method for handling a user without a defuse card -> out of game
     * @return protocol message for when a player draws an exploding kitten
     */
    @Override
    public String doExplode() {
        return ProtocolMessages.DO_EXPLODE + ProtocolMessages.DELIMITER;
    }

    /** Method for inserting an exploding kitten in the deck
     * @param deckIndex index of the place in the deck where the card should be inserted
     * @return protocol message for inserting an exploding kitten in the deck
     */
    @Override
    public String insertExplode(int deckIndex) {
        return ProtocolMessages.INSERT_EXPLODE + ProtocolMessages.DELIMITER + deckIndex;
    }

    /** Method for choosing to whom the favour card is aimed at
     * @param playerName name of the player whom the favour card is aimed at
     * @return protocol message for choosing the player for the favour card
     */
    @Override
    public String chooseFavor(String playerName){
        return ProtocolMessages.CHOOSE_FAVOUR + ProtocolMessages.DELIMITER + playerName;
    }

    /** Method for handling the favour card
     * @param chosenCard the card that was chosen to give for the favour
     * @param fromName name of the player giving the card
     * @return protocol message for handling a favour card
     */
    @Override
    public String handleRequestFavor(int chosenCard, String fromName) {
        return ProtocolMessages.HANDLE_REQUEST_FAVOUR + ProtocolMessages.DELIMITER + chosenCard + ProtocolMessages.DELIMITER + fromName;
    }

    /** Method for handling the response to noping
     * @param isUsed boolean for whether the nope card was played or not
     * @return protocol message for handling requesting a nope card
     */
    @Override
    public String requestNopeResponse(boolean isUsed) {
        return ProtocolMessages.HANDLE_REQUEST_NOPE + ProtocolMessages.DELIMITER + isUsed;
    }

    /** Method for selecting a name
     * @param name the name of the client
     * @return protocol message for selecting the name of a client
     */
    @Override
    public String selectName(String name) {
        return ProtocolMessages.SELECT_NAME + ProtocolMessages.DELIMITER + name;
    }

    public static void main(String[] args) {
        (new GameClient()).start();
    }

}