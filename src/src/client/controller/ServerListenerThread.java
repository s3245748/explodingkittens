package client.controller;

import client.view.GameClientTUI;
import exceptions.ServerUnavailableException;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * ServerListenerThread for Networked Game Exploding Kittens
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */

public class ServerListenerThread extends Thread{
    private BufferedReader in;
    private GameClientTUI gameClientTUI;

    /**
     * Constructor for the ServerListenerThread. Initializes in and gameClientTUI
     * @param in a BufferedReader to read the input stream
     * @param gameClientTUI of which the handleServerInput() method can be called later
     *
     */
    public ServerListenerThread(BufferedReader in, GameClientTUI gameClientTUI ){
        this.in = in;
        this.gameClientTUI = gameClientTUI;
    }

    /**
     * Method that constantly checks if there is user input and if so calls the handleServerInput method in gameClientTUI
     */
    @Override
    public void run(){
        try {
            while (true){
                String mess = in.readLine();
                if (mess != null){
                    gameClientTUI.handleServerInput(mess);
                }
                else {
                    break;
                }
            }
        } catch (IOException | ServerUnavailableException e) {
            System.out.println("Error while listening to serverClient.server: " + e);
        }
    }
}
