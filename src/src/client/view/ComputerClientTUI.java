package client.view;

import client.controller.ComputerClient;
import exceptions.ServerUnavailableException;
import protocol.ProtocolMessages;

import java.util.Random;

import static protocol.ProtocolMessages.*;

public class ComputerClientTUI extends GameClientTUI{
    private ComputerClient computerClient;
    Random random;

    public ComputerClientTUI(ComputerClient gameC) {
        super(gameC);
        this.random = new Random();
    }

    @Override
    public void handleServerInput(String message) throws ServerUnavailableException {
        String[] sptMsg = message.split(ProtocolMessages.DELIMITER);
        switch (sptMsg[0]) {
            case REQUEST_ACTION:
                String[] arr1 = makePossibleIntArray(getArrLength(sptMsg[1]));
                String[] arr2 = makePoss(buildArray(sptMsg[1]));
                String[] possible = fuseArrays(arr1, arr2);
                printArrayWithIndex(buildArray(sptMsg[1]));
                System.out.println(choicesOfMultiple(arr2));
                String errorMessage = "Input must be in range 1 to " + getArrLength(sptMsg[1]) + " or \"d\" to draw, \"c2\" for a combo of 2, \"c3\" for a combo of 3";
                String move = selectRandom(possible);
                int moveInt = returnAppropriateInt(move);
                computerClient.sendMessage(computerClient.playCard(moveInt, 0));
                break;
            case HANDLE_PLAY_COMBO:
                String[] players = buildArray(sptMsg[1]);
                String[] abbreviations = buildArray(sptMsg[2]);
                printArrayWithIndex(abbreviations);
                int choseCard = random.nextInt(12)+1;
                String type = abbreviations[choseCard-1];
                computerClient.sendMessage(computerClient.executeCombo3(sptMsg[1], type));
                break;
            case EXPLODE_PLAYER:
                if (sptMsg[1].equals("true")) {
                    System.out.println(("Sorry, no defuse Card. BOOOOOM! You are out of the game!!!"));
                    computerClient.sendMessage(computerClient.doExplode());
                } else {
                    computerClient.sendMessage(computerClient.insertExplode(0));
                }
                break;
            case REQUEST_NOPE:
                computerClient.sendMessage(computerClient.requestNopeResponse(false));
                break;
            case REQUEST_NAME:
                String n = computerClient.selectName(askName()); //SN;bob
                computerClient.sendMessage(n);
                break;
            case ANNOUNCE_IN_GENERAL:
                handleServerInput(removeFirst(message));
                break;
            case DO_GENERAL_ANNOUNCE:
                handleServerInput(removeFirst(message));
                break;
            case ANNOUNCE_IN_PRIVATE:
                if (sptMsg.length == 2) {
                }
                handleServerInput(removeFirst(message));
                break;
            case ASK_FAVOUR:
                String[] playersList = buildArray(sptMsg[1]);
                printArrayWithIndex(playersList);
                int playerIndex = random.nextInt(playersList.length) + 1;
                String victimNam = playersList[playerIndex - 1];
                System.out.println("Wait for " + victimNam + " to give you a card");
                computerClient.sendMessage(computerClient.chooseFavor(victimNam));
                break;
            case REQUEST_FAVOUR:
                String[] possibleFavourInts = makePossibleIntArray(getArrLength(sptMsg[1]));
                printArrayWithIndex(buildArray(sptMsg[1]));
                String nameTo =  sptMsg[3].split("-")[0];
                int inxCard = random.nextInt(getArrLength(sptMsg[1])) + 1;
                computerClient.sendMessage(computerClient.handleRequestFavor(inxCard, nameTo));
                break;
            case DO_COMBO:
                if (sptMsg[1] != null){
                    if (sptMsg[2].equals("2")) {
                        String[] arr = buildArray(sptMsg[1]);
                        printArrayWithIndex(arr);
                        int ind = random.nextInt(getArrLength(sptMsg[1]))+ 1;
                        printArrayWithIndex(buildArray(sptMsg[3]));
                        int victimInt = random.nextInt(getArrLength(sptMsg[3])) + 1;
                        String[] playerArr = buildArray(sptMsg[3]);
                        String victimName = playerArr[victimInt - 1];
                        computerClient.sendMessage(computerClient.playCombo(2, victimName, buildArray(sptMsg[1])[ind - 1]));
                    }
                    if (sptMsg[2].equals("3")) {
                        String[] arr = buildArray(sptMsg[1]);
                        printArrayWithIndex(arr);
                        int ind = random.nextInt(getArrLength(sptMsg[1]))+1;
                        String cardType = buildArray(sptMsg[1])[ind - 1];
                        printArrayWithIndex(buildArray(sptMsg[3]));
                        int victimInt = random.nextInt(getArrLength(sptMsg[3])) + 1;
                        String[] playerArr = buildArray(sptMsg[3]);
                        System.out.println(playerArr.length);
                        String victimName = playerArr[victimInt - 1];
                        computerClient.sendMessage(computerClient.playCombo(3, victimName, cardType));
                    }
                }
                break;
            default:
                System.out.println(sptMsg[0]);
                break;
        }
    }

    /**
     * Method needed for valid computer player moves.
     * @param inputArray from which a random String will be selected
     * @return a random element in the String array
     */
    public String selectRandom(String[] inputArray){
        Random random = new Random();
        int rand = random.nextInt(inputArray.length);
        return inputArray[rand];
    }

}
