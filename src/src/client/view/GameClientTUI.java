package client.view;

import client.controller.GameClient;
import exceptions.ServerUnavailableException;
import protocol.ProtocolMessages;
import server.controller.Console;

import java.net.InetAddress;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.Scanner;
import static protocol.ProtocolMessages.*;
import java.util.*;


/**
 * GameClientTUI for Networked Game Exploding Kittens
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */

public class GameClientTUI implements GameClientView {

    private GameClient gameClient;
    private Scanner scanner;
    private Console console;

    /**
     * Constructor for the GameClientTUI
     * @param gameC the GameClient that is being used
     */
    public GameClientTUI(GameClient gameC){
        this.gameClient = gameC;
        this.scanner = new Scanner(System.in);
        this.console = new Console();
    }

    /**
     * Method which handles the server input
     * @param message the message the server sent
     * @throws ServerUnavailableException if the server connection fails
     */
    public void handleServerInput(String message) throws ServerUnavailableException {
        String[] sptMsg = message.split(ProtocolMessages.DELIMITER);
        switch (sptMsg[0]) {
            case REQUEST_ACTION:
                String[] arr1 = makePossibleIntArray(getArrLength(sptMsg[1]));
                String[] arr2 = makePoss(buildArray(sptMsg[1]));
                String[] possible = fuseArrays(arr1, arr2);
                printArrayWithIndex(buildArray(sptMsg[1]));
                System.out.println(choicesOfMultiple(arr2));
                String errorMessage = "Input must be in range 1 to " + getArrLength(sptMsg[1]) + " or \"d\" to draw, \"c2\" for a combo of 2, \"c3\" for a combo of 3";
                String move = console.expectString("Select an index of a card you want to play.\nTo draw: \"d\". To play combo of 2: \"c2\". To play combo of 3: \"c3\" ", possible, errorMessage);
                int moveInt = returnAppropriateInt(move);
                gameClient.sendMessage(gameClient.playCard(moveInt, 0));
                break;
            case HANDLE_PLAY_COMBO:
                String[] players = buildArray(sptMsg[1]);
                String[] abbreviations = buildArray(sptMsg[2]);
                printArrayWithIndex(abbreviations);
                int choseCard = console.expectInt("You may chose a card that you would want", 1, 12);
                String type = abbreviations[choseCard-1];
                gameClient.sendMessage(gameClient.executeCombo3(sptMsg[1], type));
                break;
            case EXPLODE_PLAYER:
                if (sptMsg[1].equals("true")) {
                    System.out.println(("Sorry, no defuse Card. BOOOOOM! You are out of the game!!!"));
                    gameClient.sendMessage(gameClient.doExplode());
                } else {
                    if(gameClient.getName().equals("COMPUTER")) {
                        gameClient.sendMessage(gameClient.insertExplode(0));
                    }
                   else { int deckPos = console.expectInt("You may put the card in a chosen position in the deck. The deck has " + sptMsg[2] + " Cards. 0 is the top card. What position should the card go?", 0, Integer.parseInt(sptMsg[2]));
                    gameClient.sendMessage(gameClient.insertExplode(deckPos));
                   }
                }
                break;
            case REQUEST_NOPE:
                String ansToNope = console.expectString("" , new String[] {"y", "n"}, "Choose y or n");
                boolean ans = false;
                if (ansToNope.equals("y")){
                    ans = true;
                }
                gameClient.sendMessage(gameClient.requestNopeResponse(ans));
                break;
            case REQUEST_NAME:
                String n = gameClient.selectName(askName());
                gameClient.sendMessage(n);
                break;
            case ANNOUNCE_IN_GENERAL:
                handleServerInput(removeFirst(message));
                break;
            case DO_GENERAL_ANNOUNCE:
                handleServerInput(removeFirst(message));
                break;
            case ANNOUNCE_IN_PRIVATE:
                if (sptMsg.length == 2) {
                }
                handleServerInput(removeFirst(message));
                break;
            case ASK_FAVOUR:
                String[] playersList = buildArray(sptMsg[1]);
                printArrayWithIndex(playersList);
                int playerIndex = console.expectInt("Who do you chose to give you a card?", 1, playersList.length);
                String victimNam = playersList[playerIndex - 1];
                System.out.println("Wait for " + victimNam + " to give you a card");
                gameClient.sendMessage(gameClient.chooseFavor(victimNam));
                break;
            case REQUEST_FAVOUR:
                String[] possibleFavourInts = makePossibleIntArray(getArrLength(sptMsg[1]));
                printArrayWithIndex(buildArray(sptMsg[1]));
                String nameTo =  sptMsg[3].split("-")[0];
                if(sptMsg[3].equals("COMPUTER")){
                    gameClient.sendMessage(gameClient.handleRequestFavor(1, nameTo));
                }
                else {int inxCard = console.expectInt("What card do you want to give away?", 1, getArrLength(sptMsg[1]));
                    gameClient.sendMessage(gameClient.handleRequestFavor(inxCard, nameTo));

                }
                break;
            case DO_COMBO:
                if (sptMsg[1] != null){
                    if (sptMsg[2].equals("2")) {
                        String[] arr = buildArray(sptMsg[1]);
                        printArrayWithIndex(arr);
                        int ind = console.expectInt("What pair do you want to play?", 1, getArrLength(sptMsg[1]));
                        printArrayWithIndex(buildArray(sptMsg[3]));
                        int victimInt = console.expectInt("Who do you choose to get a random card from?", 1, getArrLength(sptMsg[3]));
                        String[] playerArr = buildArray(sptMsg[3]);
                        String victimName = playerArr[victimInt - 1];
                        gameClient.sendMessage(gameClient.playCombo(2, victimName, buildArray(sptMsg[1])[ind - 1]));
                    }
                    if (sptMsg[2].equals("3")) {
                        String[] arr = buildArray(sptMsg[1]);
                        printArrayWithIndex(arr);
                        int ind = console.expectInt("What triple do you want to play?", 1, getArrLength(sptMsg[1]));
                        String cardType = buildArray(sptMsg[1])[ind - 1];
                        printArrayWithIndex(buildArray(sptMsg[3]));
                        int victimInt = console.expectInt("Who do you choose to ask a card from", 1, getArrLength(sptMsg[3]));
                        String[] playerArr = buildArray(sptMsg[3]);
                        System.out.println(playerArr.length);
                        String victimName = playerArr[victimInt - 1];
                        gameClient.sendMessage(gameClient.playCombo(3, victimName, cardType));
                    }
                }
                break;
            default:
                System.out.println(sptMsg[0]);
                break;
        }
    }

    /**
     * Method that gets how many elements are in a subsection of the server message.
     * If an array is sent the method will return how many elements are in the array.
     * @param str a section of the server message
     * @return how many elements are in the subsection.
     */
    public int getArrLength(String str){
        return (str.split("-")).length;
    }

    /**
     * Method used to split subsections of server messages. In particular arrays
     * @param str string that will be split
     * @return a string array of elements of the input divided by "-"
     */
    public String[] buildArray(String str){
        return str.split("-");
    }

    /**
     * Method for getting possible values the user can input. (concerning integers)
     * @param num how many numbers will be in the array -1. num is the highest number
     * @return a string array of numbers
     */
    public String[] makePossibleIntArray(int num){
        String[] intArr = new String[num];
        for (int i = 0; i < num; i++) {
            int n = i+1;
            intArr[i] = "" + n;
        }
        return intArr;
    }

    /**
     * Prints an array and ads an index before each element.
     * @param arr that will be printed.
     */
    public void printArrayWithIndex(String[] arr){
        String res = "";
        for(int i = 0; i < arr.length; i++){
            int index = i + 1;
            res += ("" + index + ": " + arr[i] + "\n");
        }
        System.out.println(res);
    }

    /**
     * Fuses two arrays
     * @param intArr
     * @param other
     * @return one array that is the combination of two
     */
    public String[] fuseArrays(String[] intArr, String[] other){
        int resLength = intArr.length + other.length;
        String[] result = new String[resLength];
        for (int i = 0; i < intArr.length; i++) {
            result[i] = intArr[i];
        }
        for (int i = 0; i < other.length; i++) {
            result[intArr.length+i] = other[i];
        }
        return result;
    }

    /**
     * Converts a user move input to an integer, so it can be sent to the server.
     * @requires str.equals("c2") || str.equals("c3") || str.equals("d")
     * @param str the user input
     * @return
     */
    public int returnAppropriateInt(String str){
        if (str.equals("c2")){
            return -2;
        }
        if (str.equals("c3")){
            return -3;
        }
        if (str.equals("d")){
            return -1;
        }
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            System.out.println("Invalid input.");
        }
        return 0;
    }

    /**
     * Gives a string to inform the player what moves he/she can make
     */
    public String choicesOfMultiple(String[] arr2){
        ArrayList<String> lis = new ArrayList<>();
        for (String s : arr2){
            lis.add(s);
        }
        String res = "Options for you: d";
        if (lis.contains("c2")){
            res += ", c2";
        }
        if (lis.contains("c3")){
            res += ", c3";
        }
        return res;
    }

    /**
     * Returns if the hand of the player has at least one type of card twice
     */
    public boolean doublesExist(String[] array){
        HashSet<String> set = new HashSet<>();
        for (String ele : array){
            if(!set.add(ele)){
                return true;
            }
        }
        return false;
    }
    /**
     * Returns if the hand of the player has at least one type of card three times
     */
    public boolean triplesExist(String[] array){
        Map<String, Integer> countMap = new HashMap<>();
        for (String s : array){
            countMap.put(s, countMap.getOrDefault(s, 0) + 1);
            if (countMap.get(s) >= 3){
                return true;
            }
        }
        return false;
    }

    /**
     * Makes an array of possible moves the player can make
     */
    public String[] makePoss(String[] cardHand){
        ArrayList<String> arr = new ArrayList<>();
        arr.add("d");
        if (doublesExist(cardHand)) {
            arr.add("c2");
        }
        if (triplesExist(cardHand)) {
            arr.add("c3");
        }
        String[] str = new String[arr.size()];
        for (int i = 0; i < arr.size(); i++) {
            str[i] = arr.get(i);
        }
        return str;
    }

    /**
     * Asks the user to give a name
     * @return the entered name
     */
    public String askName() {
        return console.readString("What is your name?");
    }

    public String removeFirst(String message){
        String[] sp = message.split(DELIMITER);
        String result = "";
        for (int i = 1; i < sp.length; i++) {
            result += sp[i] + DELIMITER;
        }
        return result;
    }

    @Override
    public void showMessage(String message) {
        System.out.println("GameClientTUI: " + message);
    }

    @Override
    public void start() throws ServerUnavailableException, ProtocolException {
        try {
            while (true){

            }
        } catch (Exception e){
            showMessage(String.valueOf(ProtocolMessages.EXIT));
            System.out.println("Exiting...");
            gameClient.sendExit();
        }

    }

    @Override
    public InetAddress getIp() {
        try {
            System.out.println("Enter IP address: ");
            String input = scanner.nextLine();
            return InetAddress.getByName(input);
        } catch (Exception e) {
            System.out.println("That is not a valid IP. Try again.");
            return null;
        }
    }

    @Override
    public boolean getBoolean(String question) {
        System.out.print(question + " (yes/no) ");
        String input = scanner.next().toLowerCase();
        while (!input.equals("yes") && !input.equals("no")) {
            System.out.println("Invalid input. Please enter 'yes' or 'no'.");
            System.out.print(question + " (yes/no) ");
            input = scanner.next().toLowerCase();
        }
        return input.equals("yes");
    }



}

