package client.view;

import exceptions.ServerUnavailableException;

import java.net.InetAddress;
import java.net.ProtocolException;


/**
 * Interface for the GameClientView
 *
 * @author Christina Braun
 * @author Marit van der Valk
 */
public interface GameClientView {

    public void start() throws ServerUnavailableException, ServerUnavailableException, ProtocolException;

    /**
     * Writes the given message to standard output.
     * @param message the message to write to the standard output.
     */
    public void showMessage(String message);

    /**
     * Ask the user to input a valid IP. If it is not valid, show a message and ask
     * again.
     * @return a valid IP
     */
    public InetAddress getIp();


    /**
     * Prints the question and asks the user for a yes/no answer.
     * @param question The question to show to the user
     * @return The user input as boolean.
     */
    public boolean getBoolean(String question);

}
